DROP TABLE TURMAS CASCADE CONSTRAINTS;
DROP TABLE CURSOS CASCADE CONSTRAINTS;
DROP TABLE MINIFOGUETE CASCADE CONSTRAINTS;
DROP TABLE EQUIPES CASCADE CONSTRAINTS;
DROP TABLE ALUNOS CASCADE CONSTRAINTS;
DROP TABLE LANCAMENTOS CASCADE CONSTRAINTS;

DROP SEQUENCE SQ_TURMAS;
DROP SEQUENCE SQ_CURSOS;
DROP SEQUENCE SQ_MINIFOGUETES;
DROP SEQUENCE SQ_EQUIPES;
DROP SEQUENCE SQ_LANCAMENTOS;

CREATE TABLE ALUNOS(
ID_ALUNO NUMBER CONSTRAINT alunos_id_aluno_pk PRIMARY KEY,
Nome VARCHAR2(50) NOT NULL,
Data_nascimento date NOT NULL,
Id_equipe  NUMBER NOT NULL
);


CREATE TABLE CURSOS(
Id_curso NUMBER CONSTRAINT cursos_id_cur_pk PRIMARY KEY,
Nome VARCHAR2(50) NOT NULL,
Descricao  VARCHAR2(100) NOT NULL,
Qtd_turmas NUMBER(6) NOT NULL
);
CREATE SEQUENCE SQ_CURSOS;

CREATE TABLE EQUIPES(
Id_equipe NUMBER CONSTRAINT equipes_id_equi_pk PRIMARY KEY,
Nome VARCHAR2(50) NOT NULL,
Qtd_integrantes NUMBER NOT NULL,
Id_turma NUMBER NOT NULL
);
CREATE SEQUENCE SQ_EQUIPES;

CREATE TABLE TURMAS(
Id_turma NUMBER CONSTRAINT turmas_id_tur_pk PRIMARY KEY,
Nome VARCHAR2(50) NOT NULL,
Periodo  VARCHAR2(50) NOT NULL,
Id_curso NUMBER NOT NULL,
Qtd_equipes NUMBER(6) NOT NULL
);
CREATE SEQUENCE SQ_TURMAS;

CREATE TABLE MINIFOGUETE(
Id_minifoguete  NUMBER 		CONSTRAINT minifoguete_id_minif_pk PRIMARY KEY,
Nome 		VARCHAR2(50) 	NOT NULL,
Peso 		NUMBER 		NOT NULL,
Altura 		NUMBER 		NOT NULL,
Material 	VARCHAR2(100)   NOT NULL,
Id_equipe   NUMBER NOT NULL
);
CREATE SEQUENCE SQ_MINIFOGUETES;

CREATE TABLE LANCAMENTOS(
Id_lancamento NUMBER CONSTRAINT lancamentos_id_lanca_pk PRIMARY KEY,
Altitude_maxima NUMBER NOT NULL,
Velocidade_maxima NUMBER NOT NULL,
Tempo_propulsao NUMBER NOT NULL,
Pico_aceleracao NUMBER NOT NULL,
Aceleracao_media NUMBER NOT NULL,
Tempo_apogeu_descida NUMBER NOT NULL,
Tempo_ejecao NUMBER NOT NULL,
Altitude_ejecao NUMBER NOT NULL,
Taxa_descida NUMBER NOT NULL,
Duracao_voo NUMBER NOT NULL,
Data_lancamento date NOT NULL,
Qtd_lancamento NUMBER,
Distancia_alvo NUMBER,
Velocidade_vento NUMBER,
Angulo_lancamento NUMBER,
Id_equipe  NUMBER NOT NULL
);
CREATE SEQUENCE SQ_LANCAMENTOS;

INSERT INTO LANCAMENTOS(Id_lancamento, Altitude_maxima, Velocidade_maxima, Tempo_propulsao, Pico_aceleracao, 
    Aceleracao_media, Tempo_apogeu_descida, Tempo_ejecao, Altitude_ejecao, Taxa_descida, Duracao_voo, Data_lancamento, 
    Qtd_lancamento, Distancia_alvo, Velocidade_vento, Angulo_lancamento, Id_equipe)
values (SQ_LANCAMENTOS.NEXTVAL, 25, 10, 25, 50, 30, 50, 3, 50, 10, 20, '03/11/2017', 1, 25, 14, 180, 1);


INSERT INTO CURSOS(Id_curso, Nome, Descricao, Qtd_turmas) VALUES (SQ_CURSOS.NEXTVAL, 'Sistemas de Informa��o', 'Bacharelado em sistemas de informa��o, curso focado em an�lise e desenvolvimento de sistemas ', 6);
INSERT INTO CURSOS(Id_curso, Nome, Descricao, Qtd_turmas) VALUES (SQ_CURSOS.NEXTVAL, 'Engenharia da Computa��o', 'Bacharelado em engenharia da computa��o, curso focado em estuturas computacionais ', 0);
 
INSERT INTO ALUNOS(ID_ALUNO, Nome, Data_nascimento, Id_equipe) Values (76000,'Renan', '03/11/17', 1);

INSERT INTO EQUIPES(Id_equipe, Nome, Qtd_integrantes, Id_turma) VALUES (SQ_EQUIPES.NEXTVAL, 'Team Marvel', 5, 1);
INSERT INTO EQUIPES(Id_equipe, Nome, Qtd_integrantes, Id_turma) VALUES (SQ_EQUIPES.NEXTVAL, 'Team Oloko', 5, 1);
INSERT INTO EQUIPES(Id_equipe, Nome, Qtd_integrantes, Id_turma) VALUES (SQ_EQUIPES.NEXTVAL, 'Team Bicho', 5, 1);
INSERT INTO EQUIPES(Id_equipe, Nome, Qtd_integrantes, Id_turma) VALUES (SQ_EQUIPES.NEXTVAL, 'Team Vem', 5, 1);
INSERT INTO EQUIPES(Id_equipe, Nome, Qtd_integrantes, Id_turma) VALUES (SQ_EQUIPES.NEXTVAL, 'Team Nossa', 5, 1);
INSERT INTO EQUIPES(Id_equipe, Nome, Qtd_integrantes, Id_turma) VALUES (SQ_EQUIPES.NEXTVAL, 'Team Vai', 5, 1);
INSERT INTO EQUIPES(Id_equipe, Nome, Qtd_integrantes, Id_turma) VALUES (SQ_EQUIPES.NEXTVAL, 'Team Que isso', 5, 1);

INSERT INTO TURMAS(Id_turma, Nome, Periodo, Id_curso, Qtd_equipes) VALUES (SQ_TURMAS.NEXTVAL, '1SIT', 'Noturno', 1, 2);
INSERT INTO TURMAS(Id_turma, Nome, Periodo, Id_curso, Qtd_equipes) VALUES (SQ_TURMAS.NEXTVAL, '2SIT', 'Matutino', 1, 5);
INSERT INTO TURMAS(Id_turma, Nome, Periodo, Id_curso, Qtd_equipes) VALUES (SQ_TURMAS.NEXTVAL, '3SIT', 'Noturno', 1, 10);
INSERT INTO TURMAS(Id_turma, Nome, Periodo, Id_curso, Qtd_equipes) VALUES (SQ_TURMAS.NEXTVAL, '1SIS', 'Matutino', 1, 10);
INSERT INTO TURMAS(Id_turma, Nome, Periodo, Id_curso, Qtd_equipes) VALUES (SQ_TURMAS.NEXTVAL, '1SIR', 'Noturno', 1, 5);
INSERT INTO TURMAS(Id_turma, Nome, Periodo, Id_curso, Qtd_equipes) VALUES (SQ_TURMAS.NEXTVAL, '3SIT', 'Matutino', 1, 2);


INSERT INTO MINIFOGUETE(Id_minifoguete, Nome, Peso, Altura, Material, Id_equipe) VALUES (SQ_MINIFOGUETES.NEXTVAL, 'arrasa quarte', 6, 10, 'Metal', 1);
INSERT INTO MINIFOGUETE(Id_minifoguete, Nome, Peso, Altura, Material, Id_equipe) VALUES (SQ_MINIFOGUETES.NEXTVAL, 'Star treck', 2, 7, 'Pl�stico', 2);
INSERT INTO MINIFOGUETE(Id_minifoguete, Nome, Peso, Altura, Material, Id_equipe) VALUES (SQ_MINIFOGUETES.NEXTVAL, 'Nostromus', 4, 9, 'Metal', 3);
INSERT INTO MINIFOGUETE(Id_minifoguete, Nome, Peso, Altura, Material, Id_equipe) VALUES (SQ_MINIFOGUETES.NEXTVAL, 'Milenium', 6, 10.3, 'Metal', 4);
INSERT INTO MINIFOGUETE(Id_minifoguete, Nome, Peso, Altura, Material, Id_equipe) VALUES (SQ_MINIFOGUETES.NEXTVAL, 'Falcon', 6, 10.4, 'Metal', 5);
INSERT INTO MINIFOGUETE(Id_minifoguete, Nome, Peso, Altura, Material, Id_equipe) VALUES (SQ_MINIFOGUETES.NEXTVAL, 'ON-34', 6, 10.5, 'Metal', 6);



ALTER TABLE ALUNOS ADD CONSTRAINT equipes_id_equipes_fk FOREIGN KEY(Id_equipe) REFERENCES EQUIPES(Id_equipe) ON DELETE CASCADE;

ALTER TABLE EQUIPES ADD CONSTRAINT equipes_id_turma_fk FOREIGN KEY(Id_turma) REFERENCES TURMAS(Id_turma) ON DELETE CASCADE;

ALTER TABLE MINIFOGUETE ADD CONSTRAINT FOGUETE_ID_EQUIPES_FK FOREIGN KEY(ID_EQUIPE) REFERENCES EQUIPES(Id_equipe) ON DELETE CASCADE;

ALTER TABLE TURMAS ADD CONSTRAINT equipes_id_curso_fk FOREIGN KEY (Id_curso) REFERENCES CURSOS(Id_curso) ON DELETE CASCADE;

ALTER TABLE LANCAMENTOS ADD CONSTRAINT equipe_id_equipes_fk FOREIGN KEY(Id_equipe) REFERENCES EQUIPES(Id_equipe) ON DELETE CASCADE;