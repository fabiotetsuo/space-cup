package br.com.fiap.main;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import br.com.fiap.beans.Curso;
import br.com.fiap.beans.Turma;
import br.com.fiap.bo.CursoBO;

public class CursoBOTest {
	private Curso cursoModelo() {
		Turma turma = new Turma("2SIM", "Noturno", 1, new ArrayList<>());
		List<Turma> turmas = new ArrayList<>();
		turmas.add(turma);
		Curso curso = new Curso("Sistemas de Informacao 2", "Novo curso", 1);
		curso.setTurmas(turmas);
		return curso; 
	}
	@Test
	public void testeCursoBO() {
		Curso cursoInvalido = new Curso("QWERTYUIOPASDFGHJKLXXZCVBNMLKJHGFDSAQWERTYUIOPPLKJHG", "AAAA", 1);
		Curso cursoRetornado = null;
		Turma turmaNova = new Turma("2SIB", "Noturno", 0, new ArrayList<>());
		String retornoGravar = null, retornoGravar2 = null, retornoAdicionarTurma = null, 
				retornoExcluir = null, retornoRemoverTurma = null;
		long idCurso = 0, idTurma = 0;
		try {
			retornoGravar = CursoBO.novoCurso(this.cursoModelo());
			idCurso = Long.parseLong(retornoGravar.replaceAll("[^0-9]", ""));
			retornoGravar2 = CursoBO.novoCurso(cursoInvalido);
			cursoRetornado = CursoBO.consultarPorId(idCurso);
			retornoAdicionarTurma = CursoBO.adicionarTurmaNoCurso(idCurso, turmaNova);
			idTurma = Long.parseLong(retornoAdicionarTurma.replaceAll("[^0-9]", ""));
			retornoRemoverTurma = CursoBO.removerTurmaDoCurso(idCurso, idTurma);
			/*
			retornoGravar2 = TurmaBO.novaTurma(turmaInvalida, 1);
			retornoGravar3 = TurmaBO.novaTurma(turmaInvalida2, 2);
			turmaRetornada = TurmaBO.consultarPorId(idTurma);
			retornoAdicionarEquipe = TurmaBO.adicionarEquipeNaTurma(idTurma, equipe);
			
			turmas = TurmaBO.listarTurmasPorCurso(1);
			*/
			retornoExcluir = CursoBO.excluirCurso(idCurso);
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		Assert.assertEquals("Valores devem ser iguais", idCurso+" curso salvo com sucesso!", retornoGravar);
		Assert.assertEquals("Valores devem ser iguais", "Estourou o tamanho do Nome", retornoGravar2);
		Assert.assertEquals("Valores devem ser iguais", "Sistemas de Informacao 2", cursoRetornado.getNome());
		Assert.assertEquals("Valores devem ser iguais", "Novo curso", cursoRetornado.getDescricao());
		Assert.assertEquals("Valores devem ser iguais", 1, cursoRetornado.getQtdTurmas());
		Assert.assertEquals("Valores devem ser iguais", idTurma+" turma foi adicionada", retornoAdicionarTurma);
		Assert.assertEquals("Valores devem ser iguais", "1 turma foi removida", retornoRemoverTurma);
		Assert.assertEquals("Valores devem ser iguais", "Curso excluido com sucesso!", retornoExcluir);
		
	}
}
