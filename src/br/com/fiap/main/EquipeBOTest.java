package br.com.fiap.main;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import br.com.fiap.beans.Aluno;
import br.com.fiap.beans.Equipe;
import br.com.fiap.beans.Lancamento;
import br.com.fiap.beans.MiniFoguete;
import br.com.fiap.bo.EquipeBO;

public class EquipeBOTest {
	private Equipe equipeModelo() {
		@SuppressWarnings("deprecation")
		Date date = new Date(96, 04, 28);
		Aluno a = new Aluno(76480, "Fabio Chuman", date);
		Aluno b = new Aluno(76483, "Zezinho Pingo", date);
		List<Aluno> alunos = new ArrayList<>();
		alunos.add(a);
		alunos.add(b);
		MiniFoguete foguete = new MiniFoguete("APOLLO22", "Fibra de Carbono", 1, 1);
		Lancamento lancamento = new Lancamento(50, 10, 50, 30, 10, 7, 4, 20, 2, 10, 70, 5, 1, date, 3);
		Lancamento lancamento2 = new Lancamento(60, 10, 50, 30, 10, 7, 4, 20, 2, 10, 70, 5, 1, date, 3);
		List<Lancamento> lancamentos = new ArrayList<>();
		lancamentos.add(lancamento);
		lancamentos.add(lancamento2);
		
		Equipe equipe = new Equipe("Rocket", 2, alunos, foguete, lancamentos);
		return equipe; 
	}
	@SuppressWarnings("deprecation")
	@Test
	public void testeEquipeBO() {
		List<Equipe> equipes = null;
		Date date = new Date(96, 04, 28);
		Aluno aluno3 = new Aluno(23456, "Jorgensen", date);
		Aluno aluno4 = new Aluno(12389, "Fernando", date);
		Aluno aluno5 = new Aluno(32323, "Julia", date);
		Aluno aluno6 = new Aluno(12412, "Mancini", date);
		Equipe equipeRetornada = null;
		Equipe equipeInvalida = new Equipe("Nosso Canto", 6, new ArrayList<Aluno>(), new MiniFoguete(), new ArrayList<Lancamento>());
		Equipe equipeInvalida2 = new Equipe("O rato roeu a roupa do rei de roma, o rato roeu a roupa do rei de roma", 5, new ArrayList<Aluno>(), new MiniFoguete(), new ArrayList<Lancamento>());
		String retornoGravar = null, retornoGravar2 = null, retornoGravar3 = null, retornoAdicionarAluno = null, 
				retornoAdicionarAluno2 = null, retornoAdicionarAluno3 = null, retornoAdicionarAluno4 = null, retornoExcluir = null, 
				retornoRemoverAluno = null, retornoRemoverAluno2 = null, retornoRemoverAluno3 = null, retornoRemoverAluno4 = null, 
				retornoRemoverAluno5 = null;
		long idEquipe = 0;
		try {
			retornoGravar = EquipeBO.novaEquipe(this.equipeModelo(), 1);
			idEquipe = Long.parseLong(retornoGravar.replaceAll("[^0-9]", ""));
			retornoGravar2 = EquipeBO.novaEquipe(equipeInvalida, 1);
			retornoGravar3 = EquipeBO.novaEquipe(equipeInvalida2, 2);
			equipeRetornada = EquipeBO.consultarPorId(idEquipe);
			retornoAdicionarAluno = EquipeBO.adicionarAlunoNaEquipe(idEquipe, aluno3);
			retornoAdicionarAluno2 = EquipeBO.adicionarAlunoNaEquipe(idEquipe, aluno4);
			retornoAdicionarAluno3 = EquipeBO.adicionarAlunoNaEquipe(idEquipe, aluno5);
			retornoAdicionarAluno4 = EquipeBO.adicionarAlunoNaEquipe(idEquipe, aluno6);
			retornoRemoverAluno = EquipeBO.removerAlunoDaEquipe(idEquipe, 23456);
			retornoRemoverAluno2 = EquipeBO.removerAlunoDaEquipe(idEquipe, 12389);
			retornoRemoverAluno3 = EquipeBO.removerAlunoDaEquipe(idEquipe, 32323);
			retornoRemoverAluno4 = EquipeBO.removerAlunoDaEquipe(idEquipe, 76483);
			retornoRemoverAluno5 = EquipeBO.removerAlunoDaEquipe(idEquipe, 76480);
			equipes = EquipeBO.listarEquipesPorTurma(1);
			System.out.println(equipes.size());
			retornoExcluir = EquipeBO.excluirEquipe(idEquipe);
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		Assert.assertEquals("Valores devem ser iguais", idEquipe+" equipe salva com sucesso!", retornoGravar);
		Assert.assertEquals("Valores devem ser iguais", "Estourou o limite de integrantes", retornoGravar2);
		Assert.assertEquals("Valores devem ser iguais", "Estourou o tamanho do Nome", retornoGravar3);
		Assert.assertEquals("Valores devem ser iguais", "Rocket", equipeRetornada.getNome());
		Assert.assertEquals("Valores devem ser iguais", 2, equipeRetornada.getQtdIntegrantes());
		Assert.assertEquals("Valores devem ser iguais", 2, equipeRetornada.getAlunos().size());
		Assert.assertEquals("Valores devem ser iguais", equipeRetornada.getQtdIntegrantes(), equipeRetornada.getAlunos().size());
		Assert.assertEquals("Valores devem ser iguais", "1 aluno foi adicionado", retornoAdicionarAluno);
		Assert.assertEquals("Valores devem ser iguais", "1 aluno foi adicionado", retornoAdicionarAluno2);
		Assert.assertEquals("Valores devem ser iguais", "1 aluno foi adicionado", retornoAdicionarAluno3);
		Assert.assertEquals("Valores devem ser iguais", "A equipe ja esta cheia", retornoAdicionarAluno4);
		Assert.assertEquals("Valores devem ser iguais", "1 aluno foi removido", retornoRemoverAluno);
		Assert.assertEquals("Valores devem ser iguais", "1 aluno foi removido", retornoRemoverAluno2);
		Assert.assertEquals("Valores devem ser iguais", "1 aluno foi removido", retornoRemoverAluno3);
		Assert.assertEquals("Valores devem ser iguais", "1 aluno foi removido", retornoRemoverAluno4);
		Assert.assertEquals("Valores devem ser iguais", "A equipe deve ter pelo menos 1 integrante", retornoRemoverAluno5);
		Assert.assertEquals("Valores devem ser iguais", 8, equipes.size());
		Assert.assertEquals("Valores devem ser iguais", "Equipe excluida com sucesso!", retornoExcluir);
		
	}

}
