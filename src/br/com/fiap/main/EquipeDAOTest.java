package br.com.fiap.main;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import br.com.fiap.beans.Aluno;
import br.com.fiap.beans.Equipe;
import br.com.fiap.beans.Lancamento;
import br.com.fiap.beans.MiniFoguete;
import br.com.fiap.dao.AlunoDAO;
import br.com.fiap.dao.EquipeDAO;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class EquipeDAOTest {
	
	private Equipe equipeCriada() {
		java.util.Date dataUtil = new java.util.Date();
		java.sql.Date dataSql = new java.sql.Date(dataUtil.getTime());
		
		Aluno aluno = new Aluno(12345, "Joao Silva", dataSql);
		Aluno aluno2 = new Aluno(12346, "Marcelo Candido", dataSql);
		List<Aluno> listaAlunos = new ArrayList<>();
		listaAlunos.add(aluno);
		listaAlunos.add(aluno2);
		
		MiniFoguete foguete = new MiniFoguete("APOLLO22", "Fibra de Carbono", 1, 1);

		Lancamento lancamento = new Lancamento(50, 10, 50, 30, 10, 7, 4, 20, 2, 10, 70, 5, 1, dataSql, 3);
		Lancamento lancamento2 = new Lancamento(60, 10, 50, 30, 10, 7, 4, 20, 2, 10, 70, 5, 1, dataSql, 3);
		List<Lancamento> lancamentos = new ArrayList<>();
		lancamentos.add(lancamento);
		lancamentos.add(lancamento2);
		
		Equipe equipe = new Equipe("Silvester Esta Alone", 2, listaAlunos, foguete, lancamentos);
		return equipe;
	}
	
	@Test
	public void testeCRUDEquipeDAO() {
		//Given
		EquipeDAO daoEquipe = null;
		Equipe equipe = this.equipeCriada(), retornoEquipe1 = null, retornoEquipe2 = null, retornoEquipe3 = null;
		
		AlunoDAO daoAluno = null;
		Aluno novoAluno = new Aluno(76480, "Fabio Chuman", new java.sql.Date(new java.util.Date().getTime()));
		
		long idEquipe = 0;
		int excluirEquipeRetorno = 0, alterarRetorno = 0, removerAlunoRetorno = 0;
		
		//When
		try {
			//Abrindo conexoes
			daoEquipe = new EquipeDAO();
			daoAluno = new AlunoDAO();
			
			//Gravando equipe - Create
			idEquipe = daoEquipe.salvar(equipe, 1);
			
			//Retornando equipe - Request
			retornoEquipe1 = daoEquipe.getEquipe(idEquipe);
			
			//Adicionando aluno a equipe - Update
			alterarRetorno = daoEquipe.adicionarAluno(idEquipe, novoAluno);
			retornoEquipe2 = daoEquipe.getEquipe(idEquipe);
			
			//Removendo aluno da equipe - Update
			removerAlunoRetorno = daoEquipe.removerAluno(idEquipe, novoAluno.getIdAluno());
			retornoEquipe3 = daoEquipe.getEquipe(idEquipe);
			
			
			//Excluindo equipe - Delete
			excluirEquipeRetorno = daoEquipe.excluir(idEquipe);
			
			
			//Fechando conexoes
			daoAluno.fechar();
			daoEquipe.fechar();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				daoEquipe.fechar();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		//Then
		//Assert.assertEquals("Valores devem ser iguais", 4, idEquipe);
		Assert.assertEquals("Os nomes das equipes devem ser iguais", "Silvester Esta Alone", retornoEquipe1.getNome());
		Assert.assertEquals("O número de integrantes devem ser iguais", 2, retornoEquipe1.getQtdIntegrantes());
		Assert.assertEquals("A lista deve conter o mesmo numero de integrantes", 2, retornoEquipe1.getAlunos().size());
		Assert.assertEquals("O retorno da alteracao deve ser 1", 1, alterarRetorno);
		Assert.assertEquals("A lista deve conter o mesmo numero de integrantes", 3, retornoEquipe2.getQtdIntegrantes());
		Assert.assertEquals("O retorno da alteracao deve ser 1", 1, removerAlunoRetorno);
		Assert.assertEquals("A lista deve conter o mesmo numero de integrantes", 2, retornoEquipe3.getQtdIntegrantes());
		Assert.assertEquals("Os valores devem ser iguais.", 1, excluirEquipeRetorno);
	}
}
