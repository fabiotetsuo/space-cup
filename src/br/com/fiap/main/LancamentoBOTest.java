package br.com.fiap.main;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import br.com.fiap.beans.Aluno;
import br.com.fiap.beans.Lancamento;
import br.com.fiap.bo.LancamentoBO;

public class LancamentoBOTest {
	private Lancamento lancamentoCriado() {
		java.util.Date dataUtil = new java.util.Date();
		java.sql.Date dataSql = new java.sql.Date(dataUtil.getTime());
		
		Aluno aluno = new Aluno(12345, "Joao Silva", dataSql);
		Aluno aluno2 = new Aluno(12346, "Marcelo Candido", dataSql);
		List<Aluno> listaAlunos = new ArrayList<>();
		listaAlunos.add(aluno);
		listaAlunos.add(aluno2);
		Lancamento lancamento = new Lancamento(50, 10, 50, 30, 10, 7, 4, 20, 2, 10, 70, 5, 1, dataSql, 3);
		
		return lancamento;
	}
	@Test
	public void test() {
		Lancamento retornoLancamento = null, retornoLancamento2 = null;
		Lancamento lancamentoAlterado = this.lancamentoCriado();
		lancamentoAlterado.setAltitudeMaxima(90);
		String retornoGravar = null, retornoGravar2 = null, retornoExcluir = null;
		long idLancamento = 0;
		List<Lancamento> lista = null;
		try {
			retornoGravar = LancamentoBO.novoLancamento(this.lancamentoCriado(), 1);
			idLancamento = Long.parseLong(retornoGravar.replaceAll("[^0-9]", ""));
			retornoLancamento = LancamentoBO.consultarPorId(idLancamento);
			retornoGravar2 = LancamentoBO.alterarLancamento(idLancamento, 1, lancamentoAlterado);
			retornoLancamento2 = LancamentoBO.consultarPorId(idLancamento);
			lista = LancamentoBO.listarLancamentosPorEquipe(1);
			retornoExcluir = LancamentoBO.excluirLancamento(idLancamento);
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		Assert.assertEquals("Valores devem ser iguais", idLancamento+" lancamento salvo com sucesso!", retornoGravar);
		Assert.assertEquals("Os valores devem ser iguais.", 50, retornoLancamento.getAltitudeMaxima(), 0.001);
		Assert.assertEquals("Os valores devem ser iguais.", 10, retornoLancamento.getVelocidadeMaxima(), 0.001);
		Assert.assertEquals("Os valores devem ser iguais.", 50, retornoLancamento.getTempoPropulsao(), 0.001);
		Assert.assertEquals("Os valores devem ser iguais.", "1 lancamento foi alterado", retornoGravar2);
		Assert.assertEquals("Os valores devem ser iguais.", 90, retornoLancamento2.getAltitudeMaxima(), 0.001);
		Assert.assertEquals("Os valores devem ser iguais.", 2, lista.size());
		Assert.assertEquals("Valores devem ser iguais", "Lancamento excluido com sucesso!", retornoExcluir);
	}

}
