package br.com.fiap.main;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import br.com.fiap.beans.Aluno;
import br.com.fiap.beans.Lancamento;
import br.com.fiap.dao.LancamentoDAO;

public class LancamentoDAOTest {
	
	private Lancamento lancamentoCriado() {
		java.util.Date dataUtil = new java.util.Date();
		java.sql.Date dataSql = new java.sql.Date(dataUtil.getTime());
		
		Aluno aluno = new Aluno(12345, "Joao Silva", dataSql);
		Aluno aluno2 = new Aluno(12346, "Marcelo Candido", dataSql);
		List<Aluno> listaAlunos = new ArrayList<>();
		listaAlunos.add(aluno);
		listaAlunos.add(aluno2);
		Lancamento lancamento = new Lancamento(50, 10, 50, 30, 10, 7, 4, 20, 2, 10, 70, 5, 1, dataSql, 3);
		
		return lancamento;
	}
	
	@Test
	public void testeCRUDLancamentoDAO() {
		//Given
		LancamentoDAO daoLancamento = null;
		Lancamento lancamento = this.lancamentoCriado(), retornoLancamento1 = null, retornoLancamento2 = null;
		Lancamento lancamento2 = this.lancamentoCriado();
		lancamento2.setAltitudeMaxima(90);
		List<Lancamento> lancamentos = null;
		long idLancamento = 0;
		int excluirLancamentoRetorno = 0, alterarRetorno = 0;
		
		//When
		try {
			//Abrindo conexoes
			daoLancamento = new LancamentoDAO();
			
			//Gravando equipe - Create
			idLancamento = daoLancamento.salvar(lancamento, 2);
			
			//Retornando equipe - Request
			retornoLancamento1 = daoLancamento.getLancamento(idLancamento);
			
			//Adicionando aluno a equipe - Update
			alterarRetorno = daoLancamento.alterarLancamento(idLancamento, 2, lancamento2);
			retornoLancamento2 = daoLancamento.getLancamento(idLancamento);
			
			lancamentos = daoLancamento.getLancamentosPorEquipe(2);
			Iterator<Lancamento> it = lancamentos.iterator();
			while(it.hasNext()) {
				Lancamento l = it.next();
				System.out.println(l.getAltitudeMaxima());
			}
			//Excluindo equipe - Delete
			excluirLancamentoRetorno = daoLancamento.excluir(idLancamento);
			
			
			//Fechando conexoes
			daoLancamento.fechar();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				daoLancamento.fechar();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		//Then
		Assert.assertNotEquals("Valor deve ser maior que 0", 0, idLancamento);
		Assert.assertEquals("Os valores devem ser iguais.", 50, retornoLancamento1.getAltitudeMaxima(), 0.001);
		Assert.assertEquals("Os valores devem ser iguais.", 10, retornoLancamento1.getVelocidadeMaxima(), 0.001);
		Assert.assertEquals("Os valores devem ser iguais.", 50, retornoLancamento1.getTempoPropulsao(), 0.001);
		Assert.assertEquals("Os valores devem ser iguais.", 1, alterarRetorno);
		Assert.assertEquals("Os valores devem ser iguais.", 90, retornoLancamento2.getAltitudeMaxima(), 0.001);
		Assert.assertEquals("Os valores devem ser iguais.", 1, lancamentos.size());
		Assert.assertEquals("Os valores devem ser iguais.", 90, lancamentos.get(0).getAltitudeMaxima(), 0.001);
		Assert.assertEquals("Os valores devem ser iguais.", 1, excluirLancamentoRetorno);
	}

}
