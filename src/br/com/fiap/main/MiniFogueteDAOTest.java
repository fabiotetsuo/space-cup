package br.com.fiap.main;

import org.junit.Assert;
import org.junit.Test;

import br.com.fiap.beans.MiniFoguete;
import br.com.fiap.dao.MiniFogueteDAO;

public class MiniFogueteDAOTest {

	private MiniFoguete fogueteCriado() {
		MiniFoguete foguete = new MiniFoguete("APOLLO22", "Fibra de Carbono", 1, 1);
		return foguete;
	}

	@Test
	public void testeCRUDMiniFogueteDAO() {
		//Given
		MiniFogueteDAO daoFoguete = null;
		MiniFoguete foguete = this.fogueteCriado(), retornoFoguete1 = null, retornoFoguete2 = null, retornoFoguete3 = null;
		MiniFoguete fogueteAlterado = this.fogueteCriado();
		fogueteAlterado.setNome("LISTERINE");
		
		long idFoguete = 0;
		int alterarRetorno = 0, removerFogueteRetorno = 0;
		
		//When
		try {
			//Abrindo conexoes
			daoFoguete = new MiniFogueteDAO();
			
			//Gravando equipe - Create
			idFoguete = daoFoguete.salvar(foguete, 7);
			
			//Retornando equipe - Request
			retornoFoguete1 = daoFoguete.getMiniFoguete(idFoguete);
			
			//Adicionando aluno a equipe - Update
			alterarRetorno = daoFoguete.alterar(idFoguete, fogueteAlterado);
			retornoFoguete2 = daoFoguete.getMiniFoguete(idFoguete);
			
			//Excluindo equipe - Delete
			removerFogueteRetorno = daoFoguete.excluir(idFoguete);
			retornoFoguete3 = daoFoguete.getMiniFoguetePorEquipe(7);
			
			//Fechando conexoes
			daoFoguete.fechar();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				daoFoguete.fechar();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		//Then
		//Assert.assertEquals("Valores devem ser iguais", 4, idEquipe);
		Assert.assertEquals("Os valores devem ser iguais", "APOLLO22", retornoFoguete1.getNome());
		Assert.assertEquals("Os valores devem ser iguais", 1, retornoFoguete1.getAltura(), 0.001);
		Assert.assertEquals("Os valores devem ser iguais", 1, retornoFoguete1.getPeso(), 0.001);
		Assert.assertEquals("Os valores devem ser iguais", "Fibra de Carbono", retornoFoguete1.getMaterial());
		Assert.assertEquals("Os valores devem ser iguais", 1, alterarRetorno);
		Assert.assertEquals("Os valores devem ser iguais", "LISTERINE", retornoFoguete2.getNome());
		Assert.assertEquals("Os valores devem ser iguais", 1, removerFogueteRetorno);
		Assert.assertEquals("Os valores devem ser iguais", null, retornoFoguete3.getNome());

	}
}
