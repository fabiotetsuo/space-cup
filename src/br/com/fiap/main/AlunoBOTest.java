package br.com.fiap.main;

import java.sql.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import br.com.fiap.beans.Aluno;
import br.com.fiap.bo.AlunoBO;

public class AlunoBOTest {
	private Aluno alunoModelo() {
		@SuppressWarnings("deprecation")
		Date date = new Date(96, 04, 28);
		Aluno a = new Aluno(76480, "Fabio Chuman", date);
		return a; 
	}
	@SuppressWarnings("deprecation")
	@Test
	public void testeAlunoBO() {
		Aluno alunoCriado = this.alunoModelo(), retornoConsulta = null, retornoConsulta2 = null, retornoConsulta3 = null;
		Date date = new Date(28, 05, 96);
		Aluno alunoRmInvalido = new Aluno(0, "Joaozinho", date);
		Aluno alunoNomeInvalido = new Aluno(12345, "Maria dos Santos Rita Carmen Ora e Josiane Abreu Junior", date);
		Aluno alunoAlterado = new Aluno(76480, "Fabio Tetsuo", date);
		List<Aluno> alunos = null;
		String retornoGravar = null, retornoGravar2 = null, retornoGravar3 = null, retornoGravar4 = null, 
				retornoAlterar = null, retornoAlterar2 = null, retornoExcluir = null, retornoExcluir2 = null;
		try {
			//Gravando Alunos
			retornoGravar = AlunoBO.novoAluno(alunoCriado, 1);
			retornoGravar2 = AlunoBO.novoAluno(alunoRmInvalido, 1);
			retornoGravar3 = AlunoBO.novoAluno(alunoNomeInvalido, 1);
			retornoGravar4 = AlunoBO.novoAluno(alunoCriado, 1);
			
			//Consultando Alunos
			retornoConsulta = AlunoBO.consultarPorRm(76480);
			retornoConsulta2 = AlunoBO.consultarPorRm(0);
			
			//Alterando Aluno
			retornoAlterar = AlunoBO.alterarAluno(alunoAlterado);
			retornoAlterar2 = AlunoBO.alterarAluno(alunoRmInvalido);
			retornoConsulta3 = AlunoBO.consultarPorRm(76480);
			
			//Consultar por aluno por equipe
			alunos = AlunoBO.consultarPorEquipe(1);
			
			//Excluindo Alunos
			retornoExcluir = AlunoBO.excluirPorRm(alunoCriado.getIdAluno());
			retornoExcluir2 = AlunoBO.excluirPorRm(1234567);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		Assert.assertEquals("Valores devem ser iguais", "1 aluno salvo com sucesso!", retornoGravar);
		Assert.assertEquals("Valores devem ser iguais", "Numero de RM invalido", retornoGravar2);
		Assert.assertEquals("Valores devem ser iguais", "Estourou o tamanho do Nome", retornoGravar3);
		Assert.assertEquals("Valores devem ser iguais", "Aluno ja existe!", retornoGravar4);
		Assert.assertEquals("Valores devem ser iguais", "Fabio Chuman", retornoConsulta.getNome());
		Assert.assertEquals("Valores devem ser iguais", 76480, retornoConsulta.getIdAluno());
		Assert.assertEquals("Valores devem ser iguais", new Date(96,04,28), retornoConsulta.getDataNascimento());
		Assert.assertEquals("Valores devem ser iguais", null, retornoConsulta2.getNome());
		Assert.assertEquals("Valores devem ser iguais", "1 alterado com sucesso", retornoAlterar);
		Assert.assertEquals("Valores devem ser iguais", "Rm do aluno invalido", retornoAlterar2);
		Assert.assertEquals("Valores devem ser iguais", "Fabio Tetsuo", retornoConsulta3.getNome());
		Assert.assertEquals("Valores devem ser iguais", 2, alunos.size());
		Assert.assertEquals("Valores devem ser iguais", "Aluno excluido com sucesso!", retornoExcluir);
		Assert.assertEquals("Valores devem ser iguais", "Rm do aluno invalido", retornoExcluir2);
	}

}
