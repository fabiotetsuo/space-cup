package br.com.fiap.main;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import br.com.fiap.beans.Aluno;
import br.com.fiap.dao.AlunoDAO;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AlunoDAOTest {
	private Aluno alunoCriado() {
		java.util.Date dataUtil = new java.util.Date();
		java.sql.Date dataSql = new java.sql.Date(dataUtil.getTime());
		Aluno aluno = new Aluno(76480, "Fabio Chuman", dataSql);
		return aluno;
	}
	
	@Test
	public void aGravarDB() {
		//When
		int retorno = 0;
		AlunoDAO dao = null;
		Aluno aluno = this.alunoCriado();
		try {
			dao = new AlunoDAO();
			dao.excluir(aluno.getIdAluno());
			retorno = dao.salvar(aluno, 1);
			dao.fechar();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				dao.fechar();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		Assert.assertEquals("Valores devem ser iguais", 1, retorno);
	}
	
	@Test
	public void bSelecionarAluno() {
		AlunoDAO dao = null;
		Aluno esperado = this.alunoCriado(), recebido = null;
		try {
			dao = new AlunoDAO();
			recebido = dao.getAluno(76480);
			dao.fechar();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				dao.fechar();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		Assert.assertEquals(esperado.getNome(), recebido.getNome());
	}

	@Test
	public void cAtualizarAluno(){
		AlunoDAO dao = null;
		Aluno esperado = this.alunoCriado(), recebido = null;
		esperado.setNome("Lucas Viado");
		int retorno = 0;
		
		try {
			dao = new AlunoDAO();
			retorno = dao.alterar(esperado.getIdAluno(), esperado);
			recebido = dao.getAluno(esperado.getIdAluno());
			dao.fechar();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				dao.fechar();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		Assert.assertEquals(1, retorno);
		Assert.assertEquals("Lucas Viado", recebido.getNome());
	}
	
	@Test
	public void dExcluirAluno(){
		AlunoDAO dao = null;
		Aluno esperado = this.alunoCriado();
		int retorno = 0;
		try {
			dao = new AlunoDAO();
			retorno = dao.excluir(esperado.getIdAluno());
			dao.fechar();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				dao.fechar();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		Assert.assertEquals("Valores devem ser iguais", 1, retorno);
	}
}
