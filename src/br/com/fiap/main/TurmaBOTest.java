package br.com.fiap.main;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import br.com.fiap.beans.Aluno;
import br.com.fiap.beans.Equipe;
import br.com.fiap.beans.Lancamento;
import br.com.fiap.beans.MiniFoguete;
import br.com.fiap.beans.Turma;
import br.com.fiap.bo.TurmaBO;

public class TurmaBOTest {
	private Turma turmaModelo() {
		MiniFoguete foguete = new MiniFoguete("APOLLO22", "Fibra de Carbono", 1, 1);
		Equipe equipe = new Equipe("Rocket", 2, new ArrayList<>(), foguete, new ArrayList<>());
		List<Equipe> equipes = new ArrayList<>();
		equipes.add(equipe);
		Turma turma = new Turma("2SIM", "Noturno", 1, equipes);
		return turma; 
	}
	@Test
	public void testeTurmaBO() {
		List<Turma> turmas = null;
		Turma turmaInvalida = new Turma("Engenharia da Computacao", "Noturno", -1, new ArrayList<>());
		Turma turmaInvalida2 = new Turma("O rato roeu a roupa do rei de roma, o rato roeu a roupa do rei de roma", "Noturno", 5, new ArrayList<>());
		Turma turmaRetornada = null;
		@SuppressWarnings("deprecation")
		Date date = new Date(96, 04, 28);
		Aluno aluno3 = new Aluno(23456, "Jorgensen", date);
		List<Aluno> alunos = new ArrayList<>();
		alunos.add(aluno3);
		MiniFoguete foguete = new MiniFoguete("APOLLO22", "Fibra de Carbono", 1, 1);
		Lancamento lancamento = new Lancamento(50, 10, 50, 30, 10, 7, 4, 20, 2, 10, 70, 5, 1, date, 3);
		Lancamento lancamento2 = new Lancamento(60, 10, 50, 30, 10, 7, 4, 20, 2, 10, 70, 5, 1, date, 3);
		List<Lancamento> lancamentos = new ArrayList<>();
		lancamentos.add(lancamento);
		lancamentos.add(lancamento2);
		
		Equipe equipe = new Equipe("Nosso canto", 1, alunos, foguete, lancamentos);
		
		String retornoGravar = null, retornoGravar2 = null, retornoGravar3 = null, retornoAdicionarEquipe = null, 
				retornoExcluir = null, retornoRemoverEquipe = null;
		long idTurma = 0, idEquipe = 0;
		try {
			retornoGravar = TurmaBO.novaTurma(this.turmaModelo(), 1);
			idTurma = Long.parseLong(retornoGravar.replaceAll("[^0-9]", ""));
			retornoGravar2 = TurmaBO.novaTurma(turmaInvalida, 1);
			retornoGravar3 = TurmaBO.novaTurma(turmaInvalida2, 2);
			turmaRetornada = TurmaBO.consultarPorId(idTurma);
			retornoAdicionarEquipe = TurmaBO.adicionarEquipeNaTurma(idTurma, equipe);
			idEquipe = Long.parseLong(retornoAdicionarEquipe.replaceAll("[^0-9]", ""));
			retornoRemoverEquipe = TurmaBO.removerEquipeDaTurma(idTurma, idEquipe);
			turmas = TurmaBO.listarTurmasPorCurso(1);
			retornoExcluir = TurmaBO.excluirTurma(idTurma);
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		Assert.assertEquals("Valores devem ser iguais", idTurma+" turma salva com sucesso!", retornoGravar);
		Assert.assertEquals("Valores devem ser iguais", "Limite minimo de equipes", retornoGravar2);
		Assert.assertEquals("Valores devem ser iguais", "Estourou o tamanho do Nome", retornoGravar3);
		Assert.assertEquals("Valores devem ser iguais", "2SIM", turmaRetornada.getNome());
		Assert.assertEquals("Valores devem ser iguais", 1, turmaRetornada.getQtdEquipes());
		Assert.assertEquals("Valores devem ser iguais", 1, turmaRetornada.getEquipes().size());
		Assert.assertEquals("Valores devem ser iguais", turmaRetornada.getQtdEquipes(), turmaRetornada.getEquipes().size());
		Assert.assertEquals("Valores devem ser iguais", idEquipe+" equipe foi adicionada", retornoAdicionarEquipe);
		Assert.assertEquals("Valores devem ser iguais", "1 equipe foi removida", retornoRemoverEquipe);
		Assert.assertNotEquals("Valores devem ser iguais", 0, turmas.size());
		Assert.assertEquals("Valores devem ser iguais", "Turma excluida com sucesso!", retornoExcluir);
		
	}

}
