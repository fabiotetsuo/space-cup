package br.com.fiap.main;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import br.com.fiap.beans.Aluno;
import br.com.fiap.beans.Equipe;
import br.com.fiap.beans.Lancamento;
import br.com.fiap.beans.MiniFoguete;
import br.com.fiap.beans.Turma;
import br.com.fiap.dao.TurmaDAO;

public class TurmaDAOTest {
	
	private Turma turmaCriada() {
		java.util.Date dataUtil = new java.util.Date();
		java.sql.Date dataSql = new java.sql.Date(dataUtil.getTime());
		
		Aluno aluno = new Aluno(12345, "Joao Silva", dataSql);
		Aluno aluno2 = new Aluno(12346, "Marcelo Candido", dataSql);
		List<Aluno> listaAlunos = new ArrayList<>();
		listaAlunos.add(aluno);
		listaAlunos.add(aluno2);
		
		Aluno aluno3 = new Aluno(12347, "Arlindo Cruz", dataSql);
		Aluno aluno4 = new Aluno(12348, "Ricardo Oliveira", dataSql);
		List<Aluno> listaAlunos2 = new ArrayList<>();
		listaAlunos.add(aluno3);
		listaAlunos.add(aluno4);
		

		MiniFoguete foguete = new MiniFoguete("APOLLO22", "Fibra de Carbono", 1, 1);
		MiniFoguete foguete2 = new MiniFoguete("APOLLO32", "Fibra de Carbono", 1, 1);
		
		Lancamento lancamento = new Lancamento(50, 10, 50, 30, 10, 7, 4, 20, 2, 10, 70, 5, 1, dataSql, 3);
		Lancamento lancamento2 = new Lancamento(60, 10, 50, 30, 10, 7, 4, 20, 2, 10, 70, 5, 1, dataSql, 3);
		List<Lancamento> lancamentos = new ArrayList<>();
		List<Lancamento> lancamentos2 = new ArrayList<>();
		lancamentos.add(lancamento);
		lancamentos2.add(lancamento2);
		
		Equipe equipe = new Equipe("Silvester Esta Alone", 2, listaAlunos, foguete, lancamentos);
		Equipe equipe2 = new Equipe("Los Hermanos", 3, listaAlunos2, foguete2, lancamentos2);
		List<Equipe> equipes = new ArrayList<>();
		equipes.add(equipe);
		equipes.add(equipe2);
		
		Turma turma = new Turma("2SIC", "Noturno", 2, equipes);
		return turma;
	}
	@Test
	public void testeCRUDTurmaDAO() {
		//Given
		TurmaDAO daoTurma = null;
		Turma turma = this.turmaCriada(), retornoTurma1 = null, retornoTurma2 = null, retornoTurma3 = null;
		
		java.util.Date dataUtil = new java.util.Date();
		java.sql.Date dataSql = new java.sql.Date(dataUtil.getTime());
		Aluno aluno3 = new Aluno(12349, "Joanna Dark", dataSql);
		List<Aluno> listaAlunos = new ArrayList<>();
		listaAlunos.add(aluno3);

		MiniFoguete foguete = new MiniFoguete("LISTERINE", "Fibra de Carbono", 1, 1);
		
		Lancamento lancamento = new Lancamento(50, 10, 50, 30, 10, 7, 4, 20, 2, 10, 70, 5, 1, dataSql, 3);
		Lancamento lancamento2 = new Lancamento(60, 10, 50, 30, 10, 7, 4, 20, 2, 10, 70, 5, 1, dataSql, 3);
		List<Lancamento> lancamentos = new ArrayList<>();
		lancamentos.add(lancamento);
		lancamentos.add(lancamento2);
		
		
		Equipe equipe3 = new Equipe("Star Trek", 1, listaAlunos, foguete, lancamentos);
		long idTurma = 0;
		int excluirTurmaRetorno = 0;
		long alterarRetorno = 0, removerEquipeRetorno = 0;
		
		//When
		try {
			//Abrindo conexoes
			daoTurma = new TurmaDAO();
			//daoAluno = new AlunoDAO();
			
			//Gravando turma - Create
			idTurma = daoTurma.salvar(turma, 1);
			
			//Retornando turma - Request
			retornoTurma1 = daoTurma.getTurma(idTurma);
			
			//Adicionando equipe a turma - Update
			alterarRetorno = daoTurma.adicionarEquipe(idTurma, equipe3);			
			retornoTurma2 = daoTurma.getTurma(idTurma);
			
			//Removendo equipe da turma - Update
			removerEquipeRetorno = daoTurma.removerEquipe(idTurma, alterarRetorno);
			retornoTurma3 = daoTurma.getTurma(idTurma);
			
			
			//Excluindo turma - Delete
			excluirTurmaRetorno = daoTurma.excluir(idTurma);
			
			//Fechando conexoes
			daoTurma.fechar();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				daoTurma.fechar();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		//Then
		//Assert.assertEquals("Valores devem ser iguais", 4, idEquipe);
		Assert.assertEquals("Os nomes das turmas devem ser iguais", "2SIC", retornoTurma1.getNome());
		Assert.assertEquals("Os periodos devem ser iguais", "Noturno", retornoTurma1.getPeriodo());
		Assert.assertEquals("A lista deve conter o mesmo numero de equipes", 2, retornoTurma1.getEquipes().size());
		Assert.assertEquals("O numero de equipes deve ser o mesmo que o tamanho da lista de equipes", retornoTurma1.getQtdEquipes(), retornoTurma1.getEquipes().size());
		Assert.assertEquals("A lista deve conter o mesmo numero de integrantes", 3, retornoTurma2.getEquipes().size());
		Assert.assertEquals("O numero de equipes deve ser o mesmo que o tamanho da lista de equipes", retornoTurma2.getQtdEquipes(), retornoTurma2.getEquipes().size());
		Assert.assertEquals("O retorno da alteracao deve ser 1", 1, removerEquipeRetorno);
		Assert.assertEquals("A lista deve conter o mesmo numero de integrantes", 2, retornoTurma3.getEquipes().size());
		Assert.assertEquals("O numero de equipes deve ser o mesmo que o tamanho da lista de equipes", retornoTurma3.getQtdEquipes(), retornoTurma3.getEquipes().size());
		Assert.assertEquals("Os valores devem ser iguais.", 1, excluirTurmaRetorno);
	}

}
