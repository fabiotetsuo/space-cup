package br.com.fiap.main;

import org.junit.Assert;
import org.junit.Test;

import br.com.fiap.beans.MiniFoguete;
import br.com.fiap.bo.MiniFogueteBO;

public class MiniFogueteBOTest {
	
	private MiniFoguete fogueteCriado() {
		MiniFoguete foguete = new MiniFoguete("APOLLO22", "Fibra de Carbono", 1, 1);
		return foguete;
	}
	@Test
	public void testeMiniFogueteBO() {
		//Given
		MiniFoguete retornoFoguete1 = null, retornoFoguete2 = null;
		MiniFoguete fogueteAlterado = this.fogueteCriado();
		fogueteAlterado.setNome("LISTERINE");
		String retornoGravar = null, retornoGravar2 = null, retornoGravar3 = null, retornoExcluir = null;
		long idFoguete = 0;
		try {
			retornoGravar = MiniFogueteBO.novoFoguete(this.fogueteCriado(), 7);
			retornoGravar2 = MiniFogueteBO.novoFoguete(this.fogueteCriado(), 7);
			idFoguete = Long.parseLong(retornoGravar.replaceAll("[^0-9]", ""));
			retornoFoguete1 = MiniFogueteBO.getMiniFoguetePorId(idFoguete);
			retornoFoguete2 = MiniFogueteBO.getMiniFoguetePorEquipe(7);
			retornoGravar3 = MiniFogueteBO.alterarMiniFoguete(idFoguete, fogueteAlterado);
			retornoExcluir = MiniFogueteBO.excluirFoguete(idFoguete);
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		Assert.assertEquals("Valores devem ser iguais", idFoguete+" foguete salvo com sucesso!", retornoGravar);
		Assert.assertEquals("Valores devem ser iguais", "Uma equipe so pode ter 1 foguete", retornoGravar2);
		Assert.assertEquals("Valores devem ser iguais", "APOLLO22", retornoFoguete1.getNome());
		Assert.assertEquals("Valores devem ser iguais", "Fibra de Carbono", retornoFoguete1.getMaterial());
		Assert.assertEquals("Valores devem ser iguais", 1, retornoFoguete1.getAltura(), 0.001);
		Assert.assertEquals("Valores devem ser iguais", 1, retornoFoguete1.getPeso(), 0.001);
		Assert.assertEquals("Valores devem ser iguais", "APOLLO22", retornoFoguete2.getNome());
		Assert.assertEquals("Valores devem ser iguais", "Fibra de Carbono", retornoFoguete2.getMaterial());
		Assert.assertEquals("Valores devem ser iguais", 1, retornoFoguete2.getAltura(), 0.001);
		Assert.assertEquals("Valores devem ser iguais", 1, retornoFoguete2.getPeso(), 0.001);
		Assert.assertEquals("Valores devem ser iguais", "1 foguete foi alterado", retornoGravar3);
		Assert.assertEquals("Valores devem ser iguais", "Foguete excluido com sucesso!", retornoExcluir);
		
	}

}
