package br.com.fiap.beans;

import java.util.List;

public class Equipe {
	private String nome;
	private int qtdIntegrantes;
	private List<Aluno> alunos;
	private List<Lancamento> lancamentos;
	private MiniFoguete foguete;
	
	public Equipe(String nome, int qtdIntegrantes, List<Aluno> alunos, MiniFoguete foguete, List<Lancamento> lancamentos) {
		super();
		this.nome = nome;
		this.qtdIntegrantes = qtdIntegrantes;
		this.alunos = alunos;
		this.foguete = foguete;
		this.lancamentos = lancamentos;
	}
	
	public Equipe() {}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getQtdIntegrantes() {
		return qtdIntegrantes;
	}

	public void setQtdIntegrantes(int qtdIntegrantes) {
		this.qtdIntegrantes = qtdIntegrantes;
	}

	public List<Aluno> getAlunos() {
		return alunos;
	}

	public void setAlunos(List<Aluno> alunos) {
		this.alunos = alunos;
	}

	public List<Lancamento> getLancamentos() {
		return lancamentos;
	}

	public void setLancamentos(List<Lancamento> lancamentos) {
		this.lancamentos = lancamentos;
	}

	public MiniFoguete getFoguete() {
		return foguete;
	}

	public void setFoguete(MiniFoguete foguete) {
		this.foguete = foguete;
	}
	
}
