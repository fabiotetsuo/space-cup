package br.com.fiap.beans;

import java.util.List;

public class Curso {
	private String nome;
	private String descricao;
	private int qtdTurmas;
	private List<Turma> turmas;
	
	public Curso(String nome, String descricao, int qtdTurmas) {
		this.nome = nome;
		this.descricao = descricao;
		this.qtdTurmas = qtdTurmas;
	}
	public Curso(){}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public int getQtdTurmas() {
		return qtdTurmas;
	}
	public void setQtdTurmas(int qtdTurmas) {
		this.qtdTurmas = qtdTurmas;
	}
	public List<Turma> getTurmas() {
		return turmas;
	}
	public void setTurmas(List<Turma> turmas) {
		this.turmas = turmas;
	}	
}
