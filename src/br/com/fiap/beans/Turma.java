package br.com.fiap.beans;

import java.util.List;

public class Turma {
	private String nome, periodo;
	private List<Equipe> equipes;
	private int qtdEquipes;
	
	public Turma(String nome, String periodo, int qtdEquipes, List<Equipe> equipes) {
		this.nome = nome;
		this.periodo = periodo;
		this.qtdEquipes = qtdEquipes;
		this.equipes = equipes;
	}
	public Turma() {}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getPeriodo() {
		return periodo;
	}
	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}
	public int getQtdEquipes() {
		return qtdEquipes;
	}
	public void setQtdEquipes(int qtdEquipes) {
		this.qtdEquipes = qtdEquipes;
	}
	public List<Equipe> getEquipes() {
		return equipes;
	}
	public void setEquipes(List<Equipe> equipes) {
		this.equipes = equipes;
	}	
}
