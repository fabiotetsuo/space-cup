package br.com.fiap.beans;

import java.sql.Date;

public class Aluno {
	private int idAluno;
	private String nome;
	private Date dataNascimento;
	
	public String getNome() {return nome;}
	public void setNome(String nome) {this.nome = nome;}
	public int getIdAluno() {return idAluno;}
	public void setIdAluno(int idAluno) {this.idAluno = idAluno;}
	public Date getDataNascimento() {return dataNascimento;}
	public void setDataNascimento(Date dataNascimento) {this.dataNascimento = dataNascimento;}
	
	public Aluno(int idAluno, String nome, Date dataNascimento) {
		this.idAluno = idAluno;
		this.nome = nome;
		this.dataNascimento = dataNascimento;
	}
	public Aluno(){}
	
}
