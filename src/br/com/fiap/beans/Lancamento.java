package br.com.fiap.beans;

import java.sql.Date;

public class Lancamento implements Comparable<Lancamento>{
	private float altitudeMaxima, velocidadeMaxima, tempoPropulsao, picoAceleracao, aceleracaoMedia, tempoApogeuDescida, tempoEjecao, 
				  altitudeEjecao, taxaDescida, duracaoVoo, distanciaAlvo, velocidadeVento, anguloLancamento;
	private Date dataLancamento;
	private int qtdLancamento;
	
	public Lancamento() {}

	public Lancamento(float altitudeMaxima, float velocidadeMaxima, float tempoPropulsao, float picoAceleracao,
			float aceleracaoMedia, float tempoApogeuDescida, float tempoEjecao, float altitudeEjecao, float taxaDescida,
			float duracaoVoo, float distanciaAlvo, float velocidadeVento, float anguloLancamento, Date dataLancamento,
			int qtdLancamento) {
		this.altitudeMaxima = altitudeMaxima;
		this.velocidadeMaxima = velocidadeMaxima;
		this.tempoPropulsao = tempoPropulsao;
		this.picoAceleracao = picoAceleracao;
		this.aceleracaoMedia = aceleracaoMedia;
		this.tempoApogeuDescida = tempoApogeuDescida;
		this.tempoEjecao = tempoEjecao;
		this.altitudeEjecao = altitudeEjecao;
		this.taxaDescida = taxaDescida;
		this.duracaoVoo = duracaoVoo;
		this.distanciaAlvo = distanciaAlvo;
		this.velocidadeVento = velocidadeVento;
		this.anguloLancamento = anguloLancamento;
		this.dataLancamento = dataLancamento;
		this.qtdLancamento = qtdLancamento;
	}

	public float getAltitudeMaxima() {
		return altitudeMaxima;
	}

	public void setAltitudeMaxima(float altitudeMaxima) {
		this.altitudeMaxima = altitudeMaxima;
	}

	public float getVelocidadeMaxima() {
		return velocidadeMaxima;
	}

	public void setVelocidadeMaxima(float velocidadeMaxima) {
		this.velocidadeMaxima = velocidadeMaxima;
	}

	public float getTempoPropulsao() {
		return tempoPropulsao;
	}

	public void setTempoPropulsao(float tempoPropulsao) {
		this.tempoPropulsao = tempoPropulsao;
	}

	public float getPicoAceleracao() {
		return picoAceleracao;
	}

	public void setPicoAceleracao(float picoAceleracao) {
		this.picoAceleracao = picoAceleracao;
	}

	public float getAceleracaoMedia() {
		return aceleracaoMedia;
	}

	public void setAceleracaoMedia(float aceleracaoMedia) {
		this.aceleracaoMedia = aceleracaoMedia;
	}

	public float getTempoApogeuDescida() {
		return tempoApogeuDescida;
	}

	public void setTempoApogeuDescida(float tempoApogeuDescida) {
		this.tempoApogeuDescida = tempoApogeuDescida;
	}

	public float getTempoEjecao() {
		return tempoEjecao;
	}

	public void setTempoEjecao(float tempoEjecao) {
		this.tempoEjecao = tempoEjecao;
	}

	public float getAltitudeEjecao() {
		return altitudeEjecao;
	}

	public void setAltitudeEjecao(float altitudeEjecao) {
		this.altitudeEjecao = altitudeEjecao;
	}

	public float getTaxaDescida() {
		return taxaDescida;
	}

	public void setTaxaDescida(float taxaDescida) {
		this.taxaDescida = taxaDescida;
	}

	public float getDuracaoVoo() {
		return duracaoVoo;
	}

	public void setDuracaoVoo(float duracaoVoo) {
		this.duracaoVoo = duracaoVoo;
	}

	public float getDistanciaAlvo() {
		return distanciaAlvo;
	}

	public void setDistanciaAlvo(float distanciaAlvo) {
		this.distanciaAlvo = distanciaAlvo;
	}

	public float getVelocidadeVento() {
		return velocidadeVento;
	}

	public void setVelocidadeVento(float velocidadeVento) {
		this.velocidadeVento = velocidadeVento;
	}

	public float getAnguloLancamento() {
		return anguloLancamento;
	}

	public void setAnguloLancamento(float anguloLancamento) {
		this.anguloLancamento = anguloLancamento;
	}

	public Date getDataLancamento() {
		return dataLancamento;
	}

	public void setDataLancamento(Date dataLancamento) {
		this.dataLancamento = dataLancamento;
	}

	public int getQtdLancamento() {
		return qtdLancamento;
	}

	public void setQtdLancamento(int qtdLancamento) {
		this.qtdLancamento = qtdLancamento;
	}

	@Override
	public int compareTo(Lancamento o) {
		// TODO Auto-generated method stub
		if(this.aceleracaoMedia > o.aceleracaoMedia){
			return 1;
		}else if(this.aceleracaoMedia < o.aceleracaoMedia){
			return -1;
		}else{
			return 0;
		}
	}
	
}
