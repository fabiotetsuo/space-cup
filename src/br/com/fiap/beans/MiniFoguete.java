package br.com.fiap.beans;

public class MiniFoguete {
	private String nome, material;
	private float peso, altura;
	
	public MiniFoguete(String nome, String material, float peso, float altura) {
		this.nome = nome;
		this.material = material;
		this.peso = peso;
		this.altura = altura;
	}
	
	public MiniFoguete() {
		// TODO Auto-generated constructor stub
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getMaterial() {
		return material;
	}

	public void setMaterial(String material) {
		this.material = material;
	}

	public float getPeso() {
		return peso;
	}

	public void setPeso(float peso) {
		this.peso = peso;
	}

	public float getAltura() {
		return altura;
	}

	public void setAltura(float altura) {
		this.altura = altura;
	}
	
	
	
}
