package br.com.fiap.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import br.com.fiap.beans.Aluno;
import br.com.fiap.beans.Equipe;
import br.com.fiap.beans.Lancamento;
import br.com.fiap.beans.MiniFoguete;
import br.com.fiap.conexao.ConexaoDB;

public class EquipeDAO {
	private Connection con;
	private PreparedStatement estrutura;
	private ResultSet resultado;
	
	public EquipeDAO() throws Exception {
		con = new ConexaoDB().connection();
	}
	
	public void fechar() throws Exception{
		con.close();
	}
	
	public long salvar(Equipe equipe, long idTurma) throws Exception {
		Long primaryKey = null;
		estrutura = con.prepareStatement
				("INSERT INTO EQUIPES (ID_EQUIPE, NOME, QTD_INTEGRANTES, ID_TURMA) "
						+ "VALUES (SQ_EQUIPES.NEXTVAL, ?, ?, ?)", new String[]{"ID_EQUIPE"});
		estrutura.setString(1, equipe.getNome());
		estrutura.setInt(2, equipe.getQtdIntegrantes());
		estrutura.setLong(3, idTurma);
		
		if (estrutura.executeUpdate() > 0) {
			ResultSet generatedKeys = estrutura.getGeneratedKeys();
			if (null != generatedKeys && generatedKeys.next()) {
				primaryKey = generatedKeys.getLong(1);
			}
		}
		estrutura.close();
		
		AlunoDAO daoAluno = new AlunoDAO();
		for(Aluno aluno: equipe.getAlunos()){
			daoAluno.salvar(aluno, primaryKey);
		}
		daoAluno.fechar();
		
		MiniFogueteDAO daoFoguete = new MiniFogueteDAO();
		daoFoguete.salvar(equipe.getFoguete(), primaryKey);
		daoFoguete.fechar();
		
		LancamentoDAO daoLancamento = new LancamentoDAO();
		for(Lancamento lancamento: equipe.getLancamentos()) {
			daoLancamento.salvar(lancamento, primaryKey);
		}
		daoLancamento.fechar();
		
		return primaryKey;
	}
	public Equipe getEquipe(long idEquipe) throws Exception {
		estrutura = con.prepareStatement("SELECT * FROM EQUIPES WHERE ID_EQUIPE=?");
		estrutura.setLong(1, idEquipe);
		resultado = estrutura.executeQuery();
		Equipe equipe = new Equipe();
		while(resultado.next()) {
			equipe.setNome(resultado.getString("NOME"));
			equipe.setQtdIntegrantes(resultado.getInt("QTD_INTEGRANTES"));
		}
		resultado.close();
		estrutura.close();
		
		AlunoDAO dao = new AlunoDAO();
		List<Aluno> alunos = dao.getAlunosPorEquipe(idEquipe);
		dao.fechar();
		
		MiniFogueteDAO daoFoguete = new MiniFogueteDAO();
		MiniFoguete foguete = daoFoguete.getMiniFoguetePorEquipe(idEquipe);
		daoFoguete.fechar();
		equipe.setAlunos(alunos);
		equipe.setFoguete(foguete);
		
		return equipe;
	}
	
	public int adicionarAluno(long idEquipe, Aluno aluno) throws Exception {
		PreparedStatement estrutura = con.prepareStatement
				("UPDATE EQUIPES SET QTD_INTEGRANTES=QTD_INTEGRANTES+1 WHERE ID_EQUIPE=?");
		estrutura.setLong(1, idEquipe);
		int retorno = estrutura.executeUpdate();
		estrutura.close();
		
		AlunoDAO dao = new AlunoDAO();
		dao.salvar(aluno, idEquipe);
		dao.fechar();
		
		return retorno;
	}
	
	public int removerAluno(long idEquipe, int rm) throws Exception {
		PreparedStatement estrutura = con.prepareStatement
				("UPDATE EQUIPES SET QTD_INTEGRANTES=QTD_INTEGRANTES-1 WHERE ID_EQUIPE=?");
		estrutura.setLong(1, idEquipe);
		int retorno = estrutura.executeUpdate();
		estrutura.close();
		
		AlunoDAO dao = new AlunoDAO();
		dao.excluir(rm);
		dao.fechar();
		
		return retorno;
	}
	
	public int excluir(long idEquipe) throws Exception{
		PreparedStatement estrutura = con.prepareStatement("DELETE FROM EQUIPES WHERE ID_EQUIPE=?");
		estrutura.setLong(1, idEquipe);
		int x = estrutura.executeUpdate();
		estrutura.close();
		return x;
	}
	
	public List<Equipe> getEquipesPorTurma(long idTurma) throws Exception {
		estrutura = con.prepareStatement("SELECT * FROM EQUIPES WHERE ID_TURMA=?");
		estrutura.setLong(1, idTurma);
		resultado = estrutura.executeQuery();
		List<Equipe> equipes = new ArrayList<>();
		Equipe equipe = new Equipe();
		while(resultado.next()) {
			equipe.setNome(resultado.getString("NOME"));
			equipe.setQtdIntegrantes(resultado.getInt("QTD_INTEGRANTES"));
			equipes.add(equipe);
		}
		resultado.close();
		estrutura.close();
		return equipes;
	}
}
