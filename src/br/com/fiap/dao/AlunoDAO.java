package br.com.fiap.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import br.com.fiap.beans.Aluno;
import br.com.fiap.conexao.ConexaoDB;

public class AlunoDAO {
	private Connection con;
	private PreparedStatement estrutura;
	private ResultSet resultado;
	
	public AlunoDAO() throws Exception {
		con = new ConexaoDB().connection();
	}
	
	public void fechar() throws Exception{
		con.close();
	}
	
	public int salvar(Aluno aluno, long idEquipe) throws Exception {
		estrutura = con.prepareStatement
				("INSERT INTO ALUNOS (ID_ALUNO, NOME, DATA_NASCIMENTO, ID_EQUIPE) "
						+ "VALUES (?, ?, ?, ?)");
		estrutura.setInt(1, aluno.getIdAluno());
		estrutura.setString(2, aluno.getNome());
		estrutura.setDate(3, aluno.getDataNascimento());
		estrutura.setLong(4, idEquipe);
		//estrutura.setInt(5, idEquipe);
		int retorno = estrutura.executeUpdate();
		estrutura.close();
		return retorno;
	}
	
	public int alterar(int rm, Aluno aluno) throws Exception {
		PreparedStatement estrutura = con.prepareStatement
				("UPDATE ALUNOS SET NOME=?, DATA_NASCIMENTO=?, ID_EQUIPE=? WHERE ID_ALUNO=?");
		estrutura.setString(1, aluno.getNome());
		estrutura.setDate(2, aluno.getDataNascimento());
		estrutura.setInt(3, 1/*aluno.getIdEquipe*/);
		estrutura.setInt(4, rm);
		int retorno = estrutura.executeUpdate();
		estrutura.close();
		return retorno;
	}
	
	public int excluir(int rm) throws Exception {
		estrutura = con.prepareStatement("DELETE FROM ALUNOS WHERE ID_ALUNO=?");
		estrutura.setInt(1, rm);
		int retorno = estrutura.executeUpdate();
		estrutura.close();
		return retorno;
	}
	
	public Aluno getAluno(int rm) throws Exception {
		estrutura = con.prepareStatement("SELECT * FROM ALUNOS WHERE ID_ALUNO=?");
		estrutura.setInt(1, rm);
		resultado = estrutura.executeQuery();
		Aluno aluno = new Aluno();
		while(resultado.next()) {
			aluno.setIdAluno(resultado.getInt("ID_ALUNO"));
			aluno.setNome(resultado.getString("NOME"));
			aluno.setDataNascimento(resultado.getDate("DATA_NASCIMENTO"));
		}
		resultado.close();
		estrutura.close();
		return aluno;
	}
	
	public List<Aluno> getAlunosPorEquipe(long idEquipe) throws Exception {
		estrutura = con.prepareStatement("SELECT * FROM ALUNOS WHERE ID_EQUIPE=?");
		estrutura.setLong(1, idEquipe);
		resultado = estrutura.executeQuery();
		List<Aluno> alunos = new ArrayList<>();
		Aluno aluno = new Aluno();
		while(resultado.next()) {
			aluno.setIdAluno(resultado.getInt("ID_ALUNO"));
			aluno.setNome(resultado.getString("NOME"));
			aluno.setDataNascimento(resultado.getDate("DATA_NASCIMENTO"));
			alunos.add(aluno);
		}
		resultado.close();
		estrutura.close();
		return alunos;
	}
	
}
