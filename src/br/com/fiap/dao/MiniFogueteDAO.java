package br.com.fiap.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import br.com.fiap.beans.MiniFoguete;
import br.com.fiap.conexao.ConexaoDB;

public class MiniFogueteDAO {
	private Connection con;
	private PreparedStatement estrutura;
	private ResultSet resultado;
	
	public MiniFogueteDAO() throws Exception {
		con = new ConexaoDB().connection();
	}
	
	public void fechar() throws Exception{
		con.close();
	}
	
	public long salvar(MiniFoguete foguete, long idEquipe) throws Exception {
		Long primaryKey = null;
		estrutura = con.prepareStatement
				("INSERT INTO MINIFOGUETE VALUES (SQ_MINIFOGUETES.NEXTVAL, ?, ?, ?, ?, ?)", new String[] {"ID_MINIFOGUETE"});
		estrutura.setString(1, foguete.getNome());
		estrutura.setFloat(2, foguete.getPeso());
		estrutura.setFloat(3, foguete.getAltura());
		estrutura.setString(4, foguete.getMaterial());
		estrutura.setLong(5, idEquipe);
		
		if (estrutura.executeUpdate() > 0) {
			ResultSet generatedKeys = estrutura.getGeneratedKeys();
			if (null != generatedKeys && generatedKeys.next()) {
				primaryKey = generatedKeys.getLong(1);
			}
		}
		estrutura.close();
		return primaryKey;
	}
	
	public MiniFoguete getMiniFoguete(long idFoguete) throws Exception {
		estrutura = con.prepareStatement("SELECT * FROM MINIFOGUETE WHERE ID_MINIFOGUETE=?");
		estrutura.setLong(1, idFoguete);
		resultado = estrutura.executeQuery();
		MiniFoguete foguete = new MiniFoguete();
		while(resultado.next()) {
			foguete.setNome(resultado.getString("NOME"));
			foguete.setPeso(resultado.getFloat("PESO"));
			foguete.setAltura(resultado.getFloat("ALTURA"));
			foguete.setMaterial(resultado.getString("MATERIAL"));
		}
		resultado.close();
		estrutura.close();
		return foguete;
	}
	
	public int alterar(long idFoguete, MiniFoguete foguete) throws Exception {
		PreparedStatement estrutura = con.prepareStatement
				("UPDATE MINIFOGUETE SET NOME=?, PESO=?, ALTURA=?, MATERIAL=? WHERE ID_MINIFOGUETE=?");
		estrutura.setString(1, foguete.getNome());
		estrutura.setFloat(2, foguete.getPeso());
		estrutura.setFloat(3, foguete.getAltura());
		estrutura.setString(4, foguete.getMaterial());
		estrutura.setLong(5, idFoguete);
		int retorno = estrutura.executeUpdate();
		estrutura.close();
		return retorno;
	}
	
	public MiniFoguete getMiniFoguetePorEquipe(long idEquipe) throws Exception {
		estrutura = con.prepareStatement("SELECT * FROM MINIFOGUETE WHERE ID_EQUIPE=?");
		estrutura.setLong(1, idEquipe);
		resultado = estrutura.executeQuery();
		MiniFoguete foguete = new MiniFoguete();
		while(resultado.next()) {
			foguete.setNome(resultado.getString("NOME"));
			foguete.setPeso(resultado.getFloat("PESO"));
			foguete.setAltura(resultado.getFloat("ALTURA"));
			foguete.setMaterial(resultado.getString("MATERIAL"));
		}
		resultado.close();
		estrutura.close();
		return foguete;
	}
	
	public int excluir(long idFoguete) throws Exception {
		estrutura = con.prepareStatement("DELETE FROM MINIFOGUETE WHERE ID_MINIFOGUETE=?");
		estrutura.setLong(1, idFoguete);
		int retorno = estrutura.executeUpdate();
		estrutura.close();
		return retorno;
	}
	
}
