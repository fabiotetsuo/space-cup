package br.com.fiap.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import br.com.fiap.beans.Equipe;
import br.com.fiap.beans.Turma;
import br.com.fiap.conexao.ConexaoDB;

public class TurmaDAO {
	private Connection con;
	private PreparedStatement estrutura;
	private ResultSet resultado;
	
	public TurmaDAO() throws Exception {
		con = new ConexaoDB().connection();
	}
	
	public void fechar() throws Exception{
		con.close();
	}
	
	public long salvar(Turma turma, long idCurso) throws Exception {
		Long primaryKey = null;
		estrutura = con.prepareStatement
				("INSERT INTO TURMAS VALUES (SQ_TURMAS.NEXTVAL, ?, ?, ?, ?)", new String[]{"ID_TURMA"});
		estrutura.setString(1, turma.getNome());
		estrutura.setString(2, turma.getPeriodo());
		estrutura.setLong(3, idCurso);
		estrutura.setInt(4, turma.getQtdEquipes());
		
		if (estrutura.executeUpdate() > 0) {
			ResultSet generatedKeys = estrutura.getGeneratedKeys();
			if (null != generatedKeys && generatedKeys.next()) {
				primaryKey = generatedKeys.getLong(1);
			}
		}
		estrutura.close();
		
		EquipeDAO dao = new EquipeDAO();
		for(Equipe equipe: turma.getEquipes()){
			dao.salvar(equipe, primaryKey);
		}
		dao.fechar();
		return primaryKey;
	}
	
	public Turma getTurma(long idTurma) throws Exception {
		estrutura = con.prepareStatement("SELECT * FROM TURMAS WHERE ID_TURMA=?");
		estrutura.setLong(1, idTurma);
		resultado = estrutura.executeQuery();
		Turma turma = new Turma();
		while(resultado.next()) {
			turma.setNome(resultado.getString("NOME"));
			turma.setPeriodo(resultado.getString("PERIODO"));
			turma.setQtdEquipes(resultado.getInt("QTD_EQUIPES"));
		}
		resultado.close();
		estrutura.close();
		EquipeDAO dao = new EquipeDAO();
		List<Equipe> equipes = dao.getEquipesPorTurma(idTurma);
		turma.setEquipes(equipes);
		dao.fechar();
		return turma;
	}
	
	public long adicionarEquipe(long idTurma, Equipe equipe) throws Exception {
		PreparedStatement estrutura = con.prepareStatement
				("UPDATE TURMAS SET QTD_EQUIPES=QTD_EQUIPES+1 WHERE ID_TURMA=?");
		estrutura.setLong(1, idTurma);
		estrutura.executeUpdate();
		estrutura.close();
		EquipeDAO dao = new EquipeDAO();
		long retorno = dao.salvar(equipe, idTurma);
		dao.fechar();
		return retorno;
	}
	public int removerEquipe(long idTurma, long idEquipe) throws Exception {
		PreparedStatement estrutura = con.prepareStatement
				("UPDATE TURMAS SET QTD_EQUIPES=QTD_EQUIPES-1 WHERE ID_TURMA=?");
		estrutura.setLong(1, idTurma);
		estrutura.executeUpdate();
		estrutura.close();
		EquipeDAO dao = new EquipeDAO();
		int retorno = dao.excluir(idEquipe);
		dao.fechar();
		return retorno;
	}
	public int excluir(long idTurma) throws Exception{
		PreparedStatement estrutura = con.prepareStatement("DELETE FROM TURMAS WHERE ID_TURMA=?");
		estrutura.setLong(1, idTurma);
		int x = estrutura.executeUpdate();
		estrutura.close();
		return x;
	}
	public List<Turma> getTurmasPorCurso(long idCurso) throws Exception {
		estrutura = con.prepareStatement("SELECT * FROM TURMAS WHERE ID_CURSO=?");
		estrutura.setLong(1, idCurso);
		resultado = estrutura.executeQuery();
		List<Turma> turmas = new ArrayList<>();
		Turma turma = new Turma();
		while(resultado.next()) {
			turma.setNome(resultado.getString("NOME"));
			turma.setPeriodo(resultado.getString("PERIODO"));
			turma.setQtdEquipes(resultado.getInt("QTD_EQUIPES"));
			turmas.add(turma);
		}
		resultado.close();
		estrutura.close();
		return turmas;
	}
}
