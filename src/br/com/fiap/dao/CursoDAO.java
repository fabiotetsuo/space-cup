package br.com.fiap.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

import br.com.fiap.beans.Curso;
import br.com.fiap.beans.Turma;
import br.com.fiap.conexao.ConexaoDB;

public class CursoDAO {
	private Connection con;
	private PreparedStatement estrutura;
	private ResultSet resultado;
	
	public CursoDAO() throws Exception {
		con = new ConexaoDB().connection();
	}
	public void fechar() throws Exception{
		con.close();
	}
	
	
	public long salvar(Curso curso) throws Exception{
		Long primaryKey = null;
		estrutura = con.prepareStatement
				("INSERT INTO CURSOS VALUES (SQ_CURSOS.NEXTVAL, ?, ?, ?)", new String[]{"ID_CURSO"});
		estrutura.setString(1, curso.getNome());
		estrutura.setString(2, curso.getDescricao());
		estrutura.setInt(3, curso.getQtdTurmas());
		
		if (estrutura.executeUpdate() > 0) {
			ResultSet generatedKeys = estrutura.getGeneratedKeys();
			if (null != generatedKeys && generatedKeys.next()) {
				primaryKey = generatedKeys.getLong(1);
			}
		}
		estrutura.close();
		
		TurmaDAO dao = new TurmaDAO();
		for(Turma turma: curso.getTurmas()){
			dao.salvar(turma, primaryKey);
		}
		dao.fechar();
		
		return primaryKey;
	}
	
	public Curso getCurso(long idCurso) throws Exception {
		estrutura = con.prepareStatement("SELECT * FROM CURSOS WHERE ID_CURSO=?");
		estrutura.setLong(1, idCurso);
		resultado = estrutura.executeQuery();
		Curso curso = new Curso();
		while(resultado.next()) {
			curso.setNome(resultado.getString("NOME"));
			curso.setDescricao(resultado.getString("DESCRICAO"));
			curso.setQtdTurmas(resultado.getInt("QTD_TURMAS"));
		}
		resultado.close();
		estrutura.close();
		TurmaDAO dao = new TurmaDAO();
		List<Turma> turmas = dao.getTurmasPorCurso(idCurso);
		curso.setTurmas(turmas);
		dao.fechar();
		return curso;
	}
	
	public long adicionarTurma(long idCurso, Turma turma) throws Exception {
		PreparedStatement estrutura = con.prepareStatement
				("UPDATE CURSOS SET QTD_TURMAS=QTD_TURMAS+1 WHERE ID_CURSO=?");
		estrutura.setLong(1, idCurso);
		estrutura.executeUpdate();
		estrutura.close();
		TurmaDAO dao = new TurmaDAO();
		long retorno = dao.salvar(turma, idCurso);
		dao.fechar();
		return retorno;
	}
	
	public int removerTurma(long idCurso, long idTurma) throws Exception {
		PreparedStatement estrutura = con.prepareStatement
				("UPDATE CURSOS SET QTD_TURMAS=QTD_TURMAS-1 WHERE ID_CURSO=?");
		estrutura.setLong(1, idCurso);
		estrutura.executeUpdate();
		estrutura.close();
		TurmaDAO dao = new TurmaDAO();
		int retorno = dao.excluir(idTurma);
		dao.fechar();
		return retorno;
	}
	
	public int excluir(long idCurso) throws Exception{
		PreparedStatement estrutura = con.prepareStatement("DELETE FROM CURSOS WHERE ID_CURSO=?");
		estrutura.setLong(1, idCurso);
		int x = estrutura.executeUpdate();
		estrutura.close();
		return x;
	}
}
