package br.com.fiap.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import br.com.fiap.beans.Lancamento;
import br.com.fiap.conexao.ConexaoDB;

public class LancamentoDAO {
	private Connection con;
	private PreparedStatement estrutura;
	private ResultSet resultado;
	
	public LancamentoDAO() throws Exception{
		// TODO Auto-generated constructor stub
		this.con = new ConexaoDB().connection();
	}
	
	public void fechar() throws Exception{
		con.close();
	}
	
	public long salvar(Lancamento lancamento, long idEquipe) throws Exception {
		Long primaryKey = null;
		estrutura = con.prepareStatement
				("INSERT INTO LANCAMENTOS VALUES (SQ_LANCAMENTOS.NEXTVAL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", new String[]{"ID_LANCAMENTO"});
		estrutura.setFloat(1, lancamento.getAltitudeMaxima());
		estrutura.setFloat(2, lancamento.getVelocidadeMaxima());
		estrutura.setFloat(3, lancamento.getTempoPropulsao());
		estrutura.setFloat(4, lancamento.getPicoAceleracao());
		estrutura.setFloat(5, lancamento.getAceleracaoMedia());
		estrutura.setFloat(6, lancamento.getTempoApogeuDescida());
		estrutura.setFloat(7, lancamento.getTempoEjecao());
		estrutura.setFloat(8, lancamento.getAltitudeEjecao());
		estrutura.setFloat(9, lancamento.getTaxaDescida());
		estrutura.setFloat(10, lancamento.getDuracaoVoo());
		estrutura.setDate(11, lancamento.getDataLancamento());
		estrutura.setInt(12, lancamento.getQtdLancamento());
		estrutura.setFloat(13, lancamento.getDistanciaAlvo());
		estrutura.setFloat(14, lancamento.getVelocidadeVento());
		estrutura.setFloat(15, lancamento.getAnguloLancamento());
		estrutura.setLong(16, idEquipe);
		
		
		if (estrutura.executeUpdate() > 0) {
			ResultSet generatedKeys = estrutura.getGeneratedKeys();
			if (null != generatedKeys && generatedKeys.next()) {
				primaryKey = generatedKeys.getLong(1);
			}
		}
		estrutura.close();
		return primaryKey;
	}
	
	public Lancamento getLancamento(long idLancamento) throws Exception {
		estrutura = con.prepareStatement("SELECT * FROM LANCAMENTOS WHERE ID_LANCAMENTO=?");
		estrutura.setLong(1, idLancamento);
		resultado = estrutura.executeQuery();
		Lancamento lancamento = new Lancamento();
		while(resultado.next()) {
			lancamento.setAltitudeMaxima(resultado.getFloat("ALTITUDE_MAXIMA"));
			lancamento.setVelocidadeMaxima(resultado.getFloat("VELOCIDADE_MAXIMA"));
			lancamento.setTempoPropulsao(resultado.getFloat("TEMPO_PROPULSAO"));
			lancamento.setPicoAceleracao(resultado.getFloat("PICO_ACELERACAO"));
			lancamento.setAceleracaoMedia(resultado.getFloat("ACELERACAO_MEDIA"));
			lancamento.setTempoApogeuDescida(resultado.getFloat("TEMPO_APOGEU_DESCIDA"));
			lancamento.setTempoEjecao(resultado.getFloat("TEMPO_EJECAO"));
			lancamento.setAltitudeEjecao(resultado.getFloat("ALTITUDE_EJECAO"));
			lancamento.setTaxaDescida(resultado.getFloat("TAXA_DESCIDA"));
			lancamento.setDuracaoVoo(resultado.getFloat("DURACAO_VOO"));
			lancamento.setDistanciaAlvo(resultado.getFloat("DISTANCIA_ALVO"));
			lancamento.setVelocidadeVento(resultado.getFloat("VELOCIDADE_VENTO"));
			lancamento.setAnguloLancamento(resultado.getFloat("ANGULO_LANCAMENTO"));
			lancamento.setDataLancamento(resultado.getDate("DATA_LANCAMENTO"));
			lancamento.setQtdLancamento(resultado.getInt("QTD_LANCAMENTO"));
		}
		resultado.close();
		estrutura.close();
		return lancamento;
	}
	
	public int alterarLancamento(long idLancamento, long idEquipe, Lancamento lancamento) throws Exception {
		PreparedStatement estrutura = con.prepareStatement
				("UPDATE LANCAMENTOS SET ALTITUDE_MAXIMA=?, VELOCIDADE_MAXIMA=?, TEMPO_PROPULSAO=?, PICO_ACELERACAO=?, \n" + 
						"    ACELERACAO_MEDIA=?, TEMPO_APOGEU_DESCIDA=?, TEMPO_EJECAO=?, ALTITUDE_EJECAO=?, TAXA_DESCIDA=?, "
						+ "DURACAO_VOO=?, DATA_LANCAMENTO=?, \n" + 
						"    QTD_LANCAMENTO=?, DISTANCIA_ALVO=?, VELOCIDADE_VENTO=?, ANGULO_LANCAMENTO=?, ID_EQUIPE=? WHERE ID_LANCAMENTO=?");
		estrutura.setFloat(1, lancamento.getAltitudeMaxima());
		estrutura.setFloat(2, lancamento.getVelocidadeMaxima());
		estrutura.setFloat(3, lancamento.getTempoPropulsao());
		estrutura.setFloat(4, lancamento.getPicoAceleracao());
		estrutura.setFloat(5, lancamento.getAceleracaoMedia());
		estrutura.setFloat(6, lancamento.getTempoApogeuDescida());
		estrutura.setFloat(7, lancamento.getTempoEjecao());
		estrutura.setFloat(8, lancamento.getAltitudeEjecao());
		estrutura.setFloat(9, lancamento.getTaxaDescida());
		estrutura.setFloat(10, lancamento.getDuracaoVoo());
		estrutura.setDate(11, lancamento.getDataLancamento());
		estrutura.setInt(12, lancamento.getQtdLancamento());
		estrutura.setFloat(13, lancamento.getDistanciaAlvo());
		estrutura.setFloat(14, lancamento.getVelocidadeVento());
		estrutura.setFloat(15, lancamento.getAnguloLancamento());
		estrutura.setLong(16, idEquipe);
		estrutura.setLong(17, idLancamento);
		int retorno = estrutura.executeUpdate();
		estrutura.close();
		return retorno;
	}
	
	public int excluir(long idLancamento) throws Exception{
		PreparedStatement estrutura = con.prepareStatement("DELETE FROM LANCAMENTOS WHERE ID_LANCAMENTO=?");
		estrutura.setLong(1, idLancamento);
		int x = estrutura.executeUpdate();
		estrutura.close();
		return x;
	}
	
	public List<Lancamento> getLancamentosPorEquipe(long idEquipe) throws Exception {
		estrutura = con.prepareStatement("SELECT * FROM LANCAMENTOS WHERE ID_EQUIPE=?");
		estrutura.setLong(1, idEquipe);
		resultado = estrutura.executeQuery();
		List<Lancamento> lancamentos = new ArrayList<>();
		Lancamento lancamento = new Lancamento();
		while(resultado.next()) {
			lancamento.setAltitudeMaxima(resultado.getFloat("ALTITUDE_MAXIMA"));
			lancamento.setVelocidadeMaxima(resultado.getFloat("VELOCIDADE_MAXIMA"));
			lancamento.setTempoPropulsao(resultado.getFloat("TEMPO_PROPULSAO"));
			lancamento.setPicoAceleracao(resultado.getFloat("PICO_ACELERACAO"));
			lancamento.setAceleracaoMedia(resultado.getFloat("ACELERACAO_MEDIA"));
			lancamento.setTempoApogeuDescida(resultado.getFloat("TEMPO_APOGEU_DESCIDA"));
			lancamento.setTempoEjecao(resultado.getFloat("TEMPO_EJECAO"));
			lancamento.setAltitudeEjecao(resultado.getFloat("ALTITUDE_EJECAO"));
			lancamento.setTaxaDescida(resultado.getFloat("TAXA_DESCIDA"));
			lancamento.setDuracaoVoo(resultado.getFloat("DURACAO_VOO"));
			lancamento.setDistanciaAlvo(resultado.getFloat("DISTANCIA_ALVO"));
			lancamento.setVelocidadeVento(resultado.getFloat("VELOCIDADE_VENTO"));
			lancamento.setAnguloLancamento(resultado.getFloat("ANGULO_LANCAMENTO"));
			lancamento.setDataLancamento(resultado.getDate("DATA_LANCAMENTO"));
			lancamento.setQtdLancamento(resultado.getInt("QTD_LANCAMENTO"));
			lancamentos.add(lancamento);
		}
		resultado.close();
		estrutura.close();
		return lancamentos;
	}	
}
