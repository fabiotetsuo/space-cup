package br.com.fiap.bo;

import java.util.ArrayList;
import java.util.List;

import br.com.fiap.beans.Lancamento;
import br.com.fiap.dao.EquipeDAO;
import br.com.fiap.dao.LancamentoDAO;

public class LancamentoBO {
	public static String novoLancamento(Lancamento obj, long idEquipe)throws Exception{
		if (obj.getAceleracaoMedia() < 0){
			return "Aceleração média não deve ser menor que 0";
		}
		if (obj.getAltitudeEjecao() < 0){
			return "Altitude de Ejeção não deve ser menor que 0";
		}
		if (obj.getAltitudeMaxima() < 0){
			return "Altitude máxima não deve ser menor que 0";
		}
		if (obj.getAnguloLancamento() < 0){
			return "Angulo de lançamento não deve ser menor que 0";
		}
		if (obj.getDistanciaAlvo() <= 0){
			return "Distância do alvo deve ser maior que 0";
		}
		if (obj.getDuracaoVoo() < 0){
			return "Duração do Vôo não deve ser menor que 0";
		}
		if (obj.getPicoAceleracao() < 0){
			return "Pico de aceleração não deve ser menor que 0";
		}
		if (obj.getQtdLancamento() < 0){
			return "Quantidade de lançamentos não deve ser menor que 0";
		}
		if (obj.getTaxaDescida() < 0){
			return "Taxa de descida não deve ser menor que 0";
		}
		if (obj.getTempoApogeuDescida() < 0){
			return "Tempo entre apogeu e descida não deve ser menor que 0";
		}
		if (obj.getTempoEjecao() < 0){
			return "Tempo de ejeção não deve ser menor que 0";
		}
		if (obj.getTempoPropulsao() < 0){
			return "Tempo de propulsão não deve ser menor que 0";
		}
		if (obj.getVelocidadeMaxima() < 0){
			return "Velocidade máxima não deve ser menor que 0";
		}
		if (obj.getVelocidadeVento() < 0){
			return "Velocidade do vento não deve ser menor que 0";
		}
		if(idEquipe<1){
			return "ID da equipe inválida";
		}
		EquipeDAO daoEquipe = new EquipeDAO();
		if(daoEquipe.getEquipe(idEquipe).getNome() == null) {
			daoEquipe.fechar();
			return "Equipe nao existe.";
		}
		daoEquipe.fechar();
		
		LancamentoDAO daoLancamento = new LancamentoDAO();
		long idLancamento = daoLancamento.salvar(obj, idEquipe);
		daoLancamento.fechar();
		return idLancamento+" lancamento salvo com sucesso!";
	}
	
	public static Lancamento consultarPorId(long idLancamento)throws Exception{
		if(idLancamento<1){
			return new Lancamento();
		}
		LancamentoDAO daoLancamento = new LancamentoDAO();
		Lancamento resp = daoLancamento.getLancamento(idLancamento);
		daoLancamento.fechar();
		return resp;
	}
	
	public static String alterarLancamento(long idLancamento, long idEquipe, Lancamento obj) throws Exception{
		if (obj.getAceleracaoMedia() < 0){
			return "Aceleração média não deve ser menor que 0";
		}
		if (obj.getAltitudeEjecao() < 0){
			return "Altitude de Ejeção não deve ser menor que 0";
		}
		if (obj.getAltitudeMaxima() < 0){
			return "Altitude máxima não deve ser menor que 0";
		}
		if (obj.getAnguloLancamento() < 0){
			return "Angulo de lançamento não deve ser menor que 0";
		}
		if (obj.getDistanciaAlvo() <= 0){
			return "Distância do alvo deve ser maior que 0";
		}
		if (obj.getDuracaoVoo() < 0){
			return "Duração do Vôo não deve ser menor que 0";
		}
		if (obj.getPicoAceleracao() < 0){
			return "Pico de aceleração não deve ser menor que 0";
		}
		if (obj.getQtdLancamento() < 0){
			return "Quantidade de lançamentos não deve ser menor que 0";
		}
		if (obj.getTaxaDescida() < 0){
			return "Taxa de descida não deve ser menor que 0";
		}
		if (obj.getTempoApogeuDescida() < 0){
			return "Tempo entre apogeu e descida não deve ser menor que 0";
		}
		if (obj.getTempoEjecao() < 0){
			return "Tempo de ejeção não deve ser menor que 0";
		}
		if (obj.getTempoPropulsao() < 0){
			return "Tempo de propulsão não deve ser menor que 0";
		}
		if (obj.getVelocidadeMaxima() < 0){
			return "Velocidade máxima não deve ser menor que 0";
		}
		if (obj.getVelocidadeVento() < 0){
			return "Velocidade do vento não deve ser menor que 0";
		}
		if(idEquipe<1){
			return "ID da equipe inválida";
		}
		
		EquipeDAO daoEquipe = new EquipeDAO();
		if(daoEquipe.getEquipe(idEquipe).getNome() == null) {
			daoEquipe.fechar();
			return "Equipe nao existe.";
		}
		daoEquipe.fechar();
		
		LancamentoDAO daoLancamento = new LancamentoDAO();
		if(daoLancamento.getLancamento(idLancamento).getAltitudeMaxima() == 0) {
			daoLancamento.fechar();
			return "Lancamento nao existe";
		}
		int retorno = daoLancamento.alterarLancamento(idLancamento, idEquipe, obj);
		daoLancamento.fechar();
		return retorno+" lancamento foi alterado";
	}
	
	public static String excluirLancamento(long idLancamento)throws Exception{
		if(idLancamento<1){
			return "Id do lancamento invalida";
		}
		LancamentoDAO daoLancamento = new LancamentoDAO();
		if(daoLancamento.getLancamento(idLancamento).getAltitudeMaxima() == 0) {
			daoLancamento.fechar();
			return "Lancamento nao existe";
		}
		daoLancamento.excluir(idLancamento);
		daoLancamento.fechar();
		return "Lancamento excluido com sucesso!";
	}
	
	public static List<Lancamento> listarLancamentosPorEquipe(long idEquipe)throws Exception{
		if(idEquipe < 1){
			return new ArrayList<Lancamento>();
		}
		LancamentoDAO daoLancamento = new LancamentoDAO();
		List<Lancamento> lista = daoLancamento.getLancamentosPorEquipe(idEquipe);
		daoLancamento.fechar();
		return lista ;
	}
}
