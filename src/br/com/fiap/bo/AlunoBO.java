package br.com.fiap.bo;

import java.util.ArrayList;
import java.util.List;

import br.com.fiap.beans.Aluno;
import br.com.fiap.dao.AlunoDAO;

public class AlunoBO {
	public static String novoAluno(Aluno obj, long idEquipe)throws Exception{
		if (obj.getIdAluno()<1 || obj.getIdAluno()>999999){
			return "Numero de RM invalido";
			//throw new RuntimeException();
		}
		if (obj.getNome().length()>50){
			return "Estourou o tamanho do Nome";
		}
		AlunoDAO dao = new AlunoDAO();
		Aluno resp = dao.getAluno(obj.getIdAluno());
		if (resp.getIdAluno()>0){
			dao.fechar();
			return "Aluno ja existe!";
		}
		long idAluno = dao.salvar(obj, idEquipe);
		dao.fechar();
		return idAluno+" aluno salvo com sucesso!";
	}
	
	public static Aluno consultarPorRm(int rm)throws Exception{
		if(rm<1 || rm>999999){
			return new Aluno();
		}
		AlunoDAO dao = new AlunoDAO();
		Aluno resp = dao.getAluno(rm);
		dao.fechar();
		return resp;
	}

	public static String alterarAluno(Aluno aluno) throws Exception{
		if(aluno.getIdAluno()<1 || aluno.getIdAluno()>999999){
			return "Rm do aluno invalido";
		}
		AlunoDAO dao = new AlunoDAO();
		int retorno = dao.alterar(aluno.getIdAluno(), aluno);
		dao.fechar();
		return retorno+" alterado com sucesso";
	}
	
	public static String excluirPorRm(int rm)throws Exception{
		if(rm<1 || rm>999999){
			return "Rm do aluno invalido";
		}
		AlunoDAO dao = new AlunoDAO();
		dao.excluir(rm);
		dao.fechar();
		return "Aluno excluido com sucesso!";
	}
	
	public static List<Aluno> consultarPorEquipe(long idEquipe)throws Exception{
		if(idEquipe < 1){
			return new ArrayList<Aluno>();
		}
		AlunoDAO dao = new AlunoDAO();
		List<Aluno> lista = dao.getAlunosPorEquipe(idEquipe);
		dao.fechar();
		return lista ;
	}
}
