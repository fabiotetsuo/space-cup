package br.com.fiap.bo;

import java.util.ArrayList;
import java.util.List;

import br.com.fiap.beans.Aluno;
import br.com.fiap.beans.Equipe;
import br.com.fiap.dao.AlunoDAO;
import br.com.fiap.dao.EquipeDAO;


public class EquipeBO {
	public static String novaEquipe(Equipe obj, long idTurma)throws Exception{
		if (obj.getNome().length()>50){
			return "Estourou o tamanho do Nome";
		}
		if (obj.getAlunos().size()<0 || obj.getAlunos().size()>5){
			return "Estourou o limite de integrantes";
		}
		if (obj.getQtdIntegrantes()<0 || obj.getQtdIntegrantes()>5) {
			return "Estourou o limite de integrantes";
		}
		if (obj.getQtdIntegrantes() != obj.getAlunos().size()) {
			return "Numero de integrantes deve ser o mesmo";
		}
		EquipeDAO dao = new EquipeDAO();
		long idEquipe = dao.salvar(obj, idTurma);
		dao.fechar();
		return idEquipe+" equipe salva com sucesso!";
	}
	
	public static Equipe consultarPorId(long idEquipe)throws Exception{
		if(idEquipe<1){
			return new Equipe();
		}
		EquipeDAO dao = new EquipeDAO();
		Equipe resp = dao.getEquipe(idEquipe);
		dao.fechar();
		return resp;
	}
	
	public static String adicionarAlunoNaEquipe(long idEquipe, Aluno aluno) throws Exception{
		if(idEquipe<1){
			return "Id da equipe invalido";
		}
		if (aluno.getIdAluno()<1 || aluno.getIdAluno()>999999){
			return "Numero de RM invalido";
		}
		if (aluno.getNome().length()>50){
			return "Estourou o tamanho do Nome";
		}
		
		//ConexaoDAO Aluno
		AlunoDAO daoAluno = new AlunoDAO();
		Aluno resp = daoAluno.getAluno(aluno.getIdAluno());
		if (resp.getIdAluno()>0){
			daoAluno.fechar();
			return "Aluno ja existe!";
		}
		daoAluno.fechar();
		
		//ConexaoDAO Equipe
		EquipeDAO daoEquipe = new EquipeDAO();
		Equipe respEquipe = daoEquipe.getEquipe(idEquipe);
		if(respEquipe.getQtdIntegrantes() >= 5) {
			daoEquipe.fechar();
			return "A equipe ja esta cheia";
		}
		int retorno = daoEquipe.adicionarAluno(idEquipe, aluno);
		daoEquipe.fechar();
		return retorno+" aluno foi adicionado";
	}
	
	public static String removerAlunoDaEquipe(long idEquipe, int rm) throws Exception{
		if(idEquipe<1){
			return "Id da equipe invalido";
		}
		if (rm<1 || rm>999999){
			return "Numero de RM invalido";
		}
		
		AlunoDAO daoAluno = new AlunoDAO();
		Aluno resp = daoAluno.getAluno(rm);
		if (resp.getIdAluno() == 0){
			daoAluno.fechar();
			return "Aluno nao existe!";
		}
		daoAluno.fechar();
		
		EquipeDAO daoEquipe = new EquipeDAO();
		Equipe respEquipe = daoEquipe.getEquipe(idEquipe);
		if(respEquipe.getQtdIntegrantes() <= 1) {
			daoEquipe.fechar();
			return "A equipe deve ter pelo menos 1 integrante";
		}
		int retorno = daoEquipe.removerAluno(idEquipe, rm);
		daoEquipe.fechar();
		return retorno+" aluno foi removido";
	}
	
	public static String excluirEquipe(long idEquipe)throws Exception{
		if(idEquipe<1){
			return "Id da equipe invalida";
		}
		EquipeDAO dao = new EquipeDAO();
		dao.excluir(idEquipe);
		dao.fechar();
		return "Equipe excluida com sucesso!";
	}
	
	public static List<Equipe> listarEquipesPorTurma(long idTurma)throws Exception{
		if(idTurma < 1){
			return new ArrayList<Equipe>();
		}
		EquipeDAO dao = new EquipeDAO();
		List<Equipe> lista = dao.getEquipesPorTurma(idTurma);
		dao.fechar();
		return lista ;
	}
}
