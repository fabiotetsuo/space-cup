package br.com.fiap.bo;

import br.com.fiap.beans.Curso;
import br.com.fiap.beans.Turma;
import br.com.fiap.dao.CursoDAO;
import br.com.fiap.dao.TurmaDAO;

public class CursoBO {
	public static String novoCurso(Curso obj)throws Exception{
		if (obj.getNome().length()>50){
			return "Estourou o tamanho do Nome";
		}
		if (obj.getDescricao().length()>100){
			return "Estourou o tamanho da descricao";
		}
		if (obj.getTurmas().size()<0){
			return "Limite minimo de turmas";
		}
		if (obj.getQtdTurmas()<0) {
			return "Limite minimo de turmas";
		}
		if (obj.getQtdTurmas() != obj.getTurmas().size()) {
			return "Numero de turmas deve ser o mesmo";
		}
		CursoDAO dao = new CursoDAO();
		long idCurso = dao.salvar(obj);
		dao.fechar();
		return idCurso+" curso salvo com sucesso!";
	}
	
	public static Curso consultarPorId(long idCurso)throws Exception{
		if(idCurso<1){
			return new Curso();
		}
		CursoDAO dao = new CursoDAO();
		Curso resp = dao.getCurso(idCurso);
		dao.fechar();
		return resp;
	}
	
	public static String adicionarTurmaNoCurso(long idCurso, Turma obj) throws Exception{
		if(idCurso<1){
			return "Id do curso invalido";
		}
		if (obj.getNome().length()>50){
			return "Estourou o tamanho do Nome";
		}
		if (obj.getPeriodo().length()>50){
			return "Estourou o tamanho do periodo";
		}
		if (obj.getEquipes().size()<0){
			return "Limite minimo de equipes";
		}
		if (obj.getQtdEquipes()<0) {
			return "Limite minimo de equipes";
		}
		if (obj.getQtdEquipes() != obj.getEquipes().size()) {
			return "Numero de equipes deve ser o mesmo";
		}
		//ConexaoDAO Equipe
		CursoDAO dao = new CursoDAO();
		long retorno = dao.adicionarTurma(idCurso, obj);
		dao.fechar();
		return retorno+" turma foi adicionada";
	}
	
	public static String removerTurmaDoCurso(long idCurso, long idTurma) throws Exception{
		if(idCurso<1){
			return "Id da turma invalida";
		}
		if (idTurma<1){
			return "Id da equipe invalida";
		}
		
		TurmaDAO daoTurma = new TurmaDAO();
		Turma resp = daoTurma.getTurma(idTurma);
		if (resp.getNome() == null){
			daoTurma.fechar();
			return "Turma nao existe!";
		}
		daoTurma.fechar();
		
		CursoDAO daoCurso = new CursoDAO();
		int retorno = daoCurso.removerTurma(idCurso, idTurma);
		daoCurso.fechar();
		return retorno+" turma foi removida";
	}
	
	public static String excluirCurso(long idCurso)throws Exception{
		if(idCurso<1){
			return "Id do curso invalido";
		}
		CursoDAO dao = new CursoDAO();
		dao.excluir(idCurso);
		dao.fechar();
		return "Curso excluido com sucesso!";
	}
}
