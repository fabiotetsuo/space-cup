package br.com.fiap.bo;

import java.util.ArrayList;
import java.util.List;

import br.com.fiap.beans.Equipe;
import br.com.fiap.beans.Turma;
import br.com.fiap.dao.EquipeDAO;
import br.com.fiap.dao.TurmaDAO;

public class TurmaBO {
	public static String novaTurma(Turma obj, long idCurso)throws Exception{
		if (obj.getNome().length()>50){
			return "Estourou o tamanho do Nome";
		}
		if (obj.getPeriodo().length()>50){
			return "Estourou o tamanho do periodo";
		}
		if (obj.getEquipes().size()<0){
			return "Limite minimo de equipes";
		}
		if (obj.getQtdEquipes()<0) {
			return "Limite minimo de equipes";
		}
		if (obj.getQtdEquipes() != obj.getEquipes().size()) {
			return "Numero de equipes deve ser o mesmo";
		}
		TurmaDAO dao = new TurmaDAO();
		long idTurma = dao.salvar(obj, idCurso);
		dao.fechar();
		return idTurma+" turma salva com sucesso!";
	}
	
	public static Turma consultarPorId(long idTurma)throws Exception{
		if(idTurma<1){
			return new Turma();
		}
		TurmaDAO dao = new TurmaDAO();
		Turma resp = dao.getTurma(idTurma);
		dao.fechar();
		return resp;
	}
	
	public static String adicionarEquipeNaTurma(long idTurma, Equipe equipe) throws Exception{
		if(idTurma<1){
			return "Id da equipe invalido";
		}
		if (equipe.getNome().length()>50){
			return "Estourou o tamanho do Nome";
		}
		if (equipe.getAlunos().size()<0 || equipe.getAlunos().size()>5){
			return "Estourou o limite de integrantes";
		}
		if (equipe.getQtdIntegrantes()<0 || equipe.getQtdIntegrantes()>5) {
			return "Estourou o limite de integrantes";
		}
		if (equipe.getQtdIntegrantes() != equipe.getAlunos().size()) {
			return "Numero de integrantes deve ser o mesmo";
		}
		
		//ConexaoDAO Equipe
		TurmaDAO dao = new TurmaDAO();
		long retorno = dao.adicionarEquipe(idTurma, equipe);
		dao.fechar();
		return retorno+" equipe foi adicionada";
	}
	
	public static String removerEquipeDaTurma(long idTurma, long idEquipe) throws Exception{
		if(idTurma<1){
			return "Id da turma invalida";
		}
		if (idEquipe<1){
			return "Id da equipe invalida";
		}
		
		EquipeDAO daoEquipe = new EquipeDAO();
		Equipe resp = daoEquipe.getEquipe(idEquipe);
		if (resp.getNome() == null){
			daoEquipe.fechar();
			return "Equipe nao existe!";
		}
		daoEquipe.fechar();
		
		TurmaDAO daoTurma = new TurmaDAO();		
		int retorno = daoTurma.removerEquipe(idTurma, idEquipe);
		daoTurma.fechar();
		return retorno+" equipe foi removida";
	}
	
	public static String excluirTurma(long idTurma)throws Exception{
		if(idTurma<1){
			return "Id da turma invalida";
		}
		TurmaDAO dao = new TurmaDAO();
		dao.excluir(idTurma);
		dao.fechar();
		return "Turma excluida com sucesso!";
	}
	
	public static List<Turma> listarTurmasPorCurso(long idCurso)throws Exception{
		if(idCurso < 1){
			return new ArrayList<Turma>();
		}
		TurmaDAO dao = new TurmaDAO();
		List<Turma> lista = dao.getTurmasPorCurso(idCurso);
		dao.fechar();
		return lista ;
	}
}
