package br.com.fiap.bo;

import br.com.fiap.beans.Equipe;
import br.com.fiap.beans.MiniFoguete;
import br.com.fiap.dao.EquipeDAO;
import br.com.fiap.dao.MiniFogueteDAO;

public class MiniFogueteBO {
	public static String novoFoguete(MiniFoguete obj, long idEquipe)throws Exception{
		if (obj.getNome().length()>50){
			return "Estourou o tamanho do Nome";
		}
		if (obj.getPeso() <= 0) {
			return "Peso deve ser maior que 0s";
		}
		if (obj.getAltura() <= 0) {
			return "Altura deve ser maior que 0s";
		}
		if (obj.getMaterial().length() > 100) {
			return "Estourou o tamanho da descricao";
		}
		if (idEquipe < 1) {
			return "ID da Equipe invalido";
		}
		
		EquipeDAO daoEquipe = new EquipeDAO();
		Equipe respEquipe = daoEquipe.getEquipe(idEquipe);
		if(respEquipe.getFoguete().getNome() != null) {
			daoEquipe.fechar();
			return "Uma equipe so pode ter 1 foguete";
		}
		if(respEquipe.getNome() == null) {
			daoEquipe.fechar();
			return "Equipe nao existe";
		}
		daoEquipe.fechar();
		
		MiniFogueteDAO dao = new MiniFogueteDAO();
		long idFoguete = dao.salvar(obj, idEquipe);
		dao.fechar();
		return idFoguete+" foguete salvo com sucesso!";
	}
	public static MiniFoguete getMiniFoguetePorId(long idFoguete) throws Exception{
		if(idFoguete<1){
			return new MiniFoguete();
		}
		
		MiniFogueteDAO daoFoguete = new MiniFogueteDAO();
		MiniFoguete resp = daoFoguete.getMiniFoguete(idFoguete);
		if (resp.getNome() == null){
			daoFoguete.fechar();
			return new MiniFoguete();
		}
		daoFoguete.fechar();
		return resp;
	}
	public static String alterarMiniFoguete(long idFoguete, MiniFoguete obj) throws Exception{
		if (obj.getNome().length()>50){
			return "Estourou o tamanho do Nome";
		}
		if (obj.getPeso() <= 0) {
			return "Peso deve ser maior que 0s";
		}
		if (obj.getAltura() <= 0) {
			return "Altura deve ser maior que 0s";
		}
		if (obj.getMaterial().length() > 100) {
			return "Estourou o tamanho da descricao";
		}
		if (idFoguete < 1) {
			return "ID do Foguete invalido";
		}
		MiniFogueteDAO daoFoguete = new MiniFogueteDAO();
		int retorno = daoFoguete.alterar(idFoguete, obj);
		daoFoguete.fechar();
		return retorno+" foguete foi alterado";
	}
	public static MiniFoguete getMiniFoguetePorEquipe(long idEquipe) throws Exception {
		if (idEquipe < 1) {
			return new MiniFoguete();
		}
		
		EquipeDAO daoEquipe = new EquipeDAO();
		Equipe respEquipe = daoEquipe.getEquipe(idEquipe);
		if(respEquipe.getNome() == null) {
			daoEquipe.fechar();
			return new MiniFoguete();
		}
		daoEquipe.fechar();
		
		MiniFogueteDAO daoFoguete = new MiniFogueteDAO();
		MiniFoguete resp = daoFoguete.getMiniFoguetePorEquipe(idEquipe);
		if (resp.getNome() == null){
			daoFoguete.fechar();
			return new MiniFoguete();
		}
		daoFoguete.fechar();
		return resp;
	}
	public static String excluirFoguete(long idFoguete)throws Exception{
		if(idFoguete<1){
			return "Id do foguete invalida";
		}
		MiniFogueteDAO dao = new MiniFogueteDAO();
		dao.excluir(idFoguete);
		dao.fechar();
		return "Foguete excluido com sucesso!";
	}
	
}
